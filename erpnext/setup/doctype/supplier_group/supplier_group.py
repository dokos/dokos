# Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt


import frappe
from frappe import _
from frappe.utils.nestedset import NestedSet, get_root_of

from erpnext.portal.utils import update_role_for_users


class SupplierGroup(NestedSet):
	nsm_parent_field = "parent_supplier_group"

	def validate(self):
		if not self.parent_supplier_group:
			self.parent_supplier_group = get_root_of("Supplier Group")
		self.validate_currency_for_payable_and_advance_account()

	def validate_currency_for_payable_and_advance_account(self):
		for x in self.accounts:
			payable_account_currency = None
			advance_account_currency = None

			if x.account:
				payable_account_currency = frappe.get_cached_value("Account", x.account, "account_currency")

			if x.advance_account:
				advance_account_currency = frappe.get_cached_value(
					"Account", x.advance_account, "account_currency"
				)

			if (
				payable_account_currency
				and advance_account_currency
				and payable_account_currency != advance_account_currency
			):
				frappe.throw(
					_(
						"Both Payable Account: {0} and Advance Account: {1} must be of same currency for company: {2}"
					).format(
						frappe.bold(x.account),
						frappe.bold(x.advance_account),
						frappe.bold(x.company),
					)
				)

	def on_update(self):
		NestedSet.on_update(self)
		self.validate_one_root()
		self.update_user_role()

	def on_trash(self):
		NestedSet.validate_if_child_exists(self)
		frappe.utils.nestedset.update_nsm(self)

	def update_user_role(self):
		supplier_has_role_profile = frappe.get_meta("Supplier").has_field("role_profile_name")
		fields = ["name", "role_profile_name"] if supplier_has_role_profile else ["name"]
		for supplier in frappe.get_all(
			"Supplier", filters={"disabled": 0, "supplier_group": self.name}, fields=fields
		):
			frappe.enqueue(
				update_role_for_users,
				doctype="Supplier",
				docname=supplier.name,
				role_profile=supplier.role_profile_name
				if supplier_has_role_profile
				else self.role_profile_name,
			)
