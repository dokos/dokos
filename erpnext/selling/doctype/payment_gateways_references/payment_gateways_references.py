# Copyright (c) 2024, Dokos SAS and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document


class PaymentGatewaysReferences(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		customer_id: DF.Data
		parent: DF.Data
		parentfield: DF.Data
		parenttype: DF.Data
		payment_gateway: DF.Link
		payment_method: DF.Data | None
		payment_method_id: DF.Data | None
	# end: auto-generated types

	pass
