from frappe import _


def get_dashboard_data(data):
	data["fieldname"] = "payment_gateway"
	data["transactions"].extend([{"label": _("Accounts"), "items": ["Payment Gateway Account"]}])

	return data
