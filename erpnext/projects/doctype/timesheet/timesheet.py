# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt


import json
from typing import Literal

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import add_to_date, flt, get_datetime, getdate
from frappe.utils.deprecations import deprecated

from erpnext.controllers.queries import get_match_cond
from erpnext.setup.utils import get_exchange_rate


class OverlapError(frappe.ValidationError):
	pass


class OverWorkLoggedError(frappe.ValidationError):
	pass


class Timesheet(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.projects.doctype.timesheet_detail.timesheet_detail import TimesheetDetail

		amended_from: DF.Link | None
		base_total_billable_amount: DF.Currency
		base_total_billed_amount: DF.Currency
		base_total_costing_amount: DF.Currency
		company: DF.Link | None
		currency: DF.Link | None
		customer: DF.Link | None
		department: DF.Link | None
		employee: DF.Link | None
		employee_name: DF.Data | None
		end_date: DF.Date | None
		exchange_rate: DF.Float
		naming_series: DF.Literal["TS-.YYYY.-"]
		note: DF.TextEditor | None
		parent_project: DF.Link | None
		per_billed: DF.Percent
		sales_invoice: DF.Link | None
		start_date: DF.Date | None
		status: DF.Literal["Draft", "Submitted", "Billed", "Payslip", "Completed", "Cancelled"]
		time_logs: DF.Table[TimesheetDetail]
		title: DF.Data | None
		total_billable_amount: DF.Currency
		total_billable_hours: DF.Float
		total_billed_amount: DF.Currency
		total_billed_hours: DF.Float
		total_costing_amount: DF.Currency
		total_hours: DF.Float
		user: DF.Link | None
	# end: auto-generated types

	def validate(self):
		self.set_status()
		self.validate_dates()
		self.validate_time_logs()
		self.update_cost()
		self.calculate_total_amounts()
		self.calculate_percentage_billed()
		self.set_dates()

	def calculate_hours(self):
		for row in self.time_logs:
			if row.to_time and row.from_time:
				row.calculate_hours()
				row.validate_billing_hours()
				row.update_billing_hours()

	def calculate_total_amounts(self):
		self.total_hours = 0.0
		self.total_billable_hours = 0.0
		self.total_billed_hours = 0.0
		self.total_billable_amount = self.base_total_billable_amount = 0.0
		self.total_costing_amount = self.base_total_costing_amount = 0.0
		self.total_billed_amount = self.base_total_billed_amount = 0.0

		for d in self.get("time_logs"):
			d.update_billing_hours()
			self.update_time_rates(d)

			self.total_hours += flt(d.hours)
			self.total_costing_amount += flt(d.costing_amount)
			self.base_total_costing_amount += flt(d.base_costing_amount)
			if d.is_billable:
				self.total_billable_hours += flt(d.billing_hours)
				self.total_billable_amount += flt(d.billing_amount)
				self.base_total_billable_amount += flt(d.base_billing_amount)
				self.total_billed_amount += flt(d.billing_amount) if d.sales_invoice else 0.0
				self.base_total_billed_amount += flt(d.base_billing_amount) if d.sales_invoice else 0.0
				self.total_billed_hours += flt(d.billing_hours) if d.sales_invoice else 0.0

	def calculate_percentage_billed(self):
		self.per_billed = 0
		if self.total_billed_amount > 0 and self.total_billable_amount > 0:
			self.per_billed = (self.total_billed_amount * 100) / self.total_billable_amount
		elif self.total_billed_hours > 0 and self.total_billable_hours > 0:
			self.per_billed = (self.total_billed_hours * 100) / self.total_billable_hours

	@deprecated
	def update_billing_hours(self, args: "TimesheetDetail"):
		args.update_billing_hours()

	def set_status(self):
		self.status = {"0": "Draft", "1": "Submitted", "2": "Cancelled"}[str(self.docstatus or 0)]

		if flt(self.per_billed, self.precision("per_billed")) >= 100.0:
			self.status = "Billed"

		if self.sales_invoice:
			self.status = "Completed"

	def set_dates(self):
		if self.docstatus < 2 and self.time_logs:
			start_date = min(getdate(d.from_time) for d in self.time_logs)
			end_date = max(getdate(d.to_time) for d in self.time_logs)

			if start_date and end_date:
				self.start_date = getdate(start_date)
				self.end_date = getdate(end_date)

	def before_cancel(self):
		self.set_status()

	def on_cancel(self):
		self.update_task_and_project()

	def on_submit(self):
		self.validate_mandatory_fields()
		self.update_task_and_project()

	def validate_mandatory_fields(self):
		for data in self.time_logs:
			if not data.from_time and not data.to_time:
				frappe.throw(_("Row {0}: From Time and To Time is mandatory.").format(data.idx))

			if not data.activity_type and self.employee:
				frappe.throw(_("Row {0}: Activity Type is mandatory.").format(data.idx))

			if flt(data.hours) == 0.0:
				frappe.throw(_("Row {0}: Hours value must be greater than zero.").format(data.idx))

	def update_task_and_project(self):
		tasks, projects = [], []

		for data in self.time_logs:
			if data.task and data.task not in tasks:
				task = frappe.get_doc("Task", data.task)
				task.update_time_and_costing()
				time_logs_completed = all(tl.completed for tl in self.time_logs if tl.task == task.name)

				if time_logs_completed:
					task.status = "Completed"
				else:
					task.status = "Working"
				task.save(ignore_permissions=True)
				tasks.append(data.task)

			if data.project and data.project not in projects:
				projects.append(data.project)

		for project in projects:
			project_doc = frappe.get_doc("Project", project)
			project_doc.update_project()
			project_doc.save(ignore_permissions=True)

	def validate_dates(self):
		for time_log in self.time_logs:
			time_log.validate_dates()

	def validate_time_logs(self):
		for time_log in self.time_logs:
			time_log.set_to_time()
			self.validate_overlap(time_log)
			time_log.set_project()
			time_log.validate_parent_project(self.parent_project)
			time_log.validate_task_project()

	def validate_overlap(self, data):
		settings = frappe.get_single("Projects Settings")
		self.validate_overlap_for("user", data, self.user, settings.ignore_user_time_overlap)
		self.validate_overlap_for("employee", data, self.employee, settings.ignore_employee_time_overlap)

	@deprecated
	def set_project(self, data: "TimesheetDetail"):
		data.set_project()

	@deprecated
	def validate_project(self, data: "TimesheetDetail"):
		data.validate_parent_project(self.parent_project)

	def validate_overlap_for(self, fieldname, args, value, ignore_validation=False):
		if not value or ignore_validation:
			return

		existing = self.get_overlap_for(fieldname, args, value)
		if existing:
			frappe.throw(
				_("Row {0}: From Time and To Time of {1} is overlapping with {2}").format(
					args.idx, self.name, existing.name
				),
				OverlapError,
			)

	def get_overlap_for(self, fieldname, args, value):
		timesheet = frappe.qb.DocType("Timesheet")
		timelog = frappe.qb.DocType("Timesheet Detail")

		from_time = get_datetime(args.from_time)
		to_time = get_datetime(args.to_time)

		existing = (
			frappe.qb.from_(timesheet)
			.join(timelog)
			.on(timelog.parent == timesheet.name)
			.select(
				timesheet.name.as_("name"), timelog.from_time.as_("from_time"), timelog.to_time.as_("to_time")
			)
			.where(
				(timelog.name != (args.name or "No Name"))
				& (timesheet.name != (args.parent or "No Name"))
				& (timesheet.docstatus < 2)
				& (timesheet[fieldname] == value)
				& (
					((from_time > timelog.from_time) & (from_time < timelog.to_time))
					| ((to_time > timelog.from_time) & (to_time < timelog.to_time))
					| ((from_time <= timelog.from_time) & (to_time >= timelog.to_time))
				)
			)
		).run(as_dict=True)

		if self.check_internal_overlap(fieldname, args):
			return self

		return existing[0] if existing else None

	def check_internal_overlap(self, fieldname, args):
		for time_log in self.time_logs:
			if not (time_log.from_time and time_log.to_time and args.from_time and args.to_time):
				continue

			from_time = get_datetime(time_log.from_time)
			to_time = get_datetime(time_log.to_time)
			args_from_time = get_datetime(args.from_time)
			args_to_time = get_datetime(args.to_time)

			if (
				(args.get(fieldname) == time_log.get(fieldname))
				and (args.idx != time_log.idx)
				and (
					(args_from_time > from_time and args_from_time < to_time)
					or (args_to_time > from_time and args_to_time < to_time)
					or (args_from_time <= from_time and args_to_time >= to_time)
				)
			):
				return True
		return False

	def update_cost(self):
		for time_log in self.time_logs:
			time_log.update_cost(self.employee)

	def update_time_rates(self, ts_detail):
		if not ts_detail.is_billable:
			ts_detail.billing_rate = 0.0

	def unlink_sales_invoice(self, sales_invoice: str):
		"""Remove link to Sales Invoice from all time logs."""
		for time_log in self.time_logs:
			if time_log.sales_invoice == sales_invoice:
				time_log.sales_invoice = None

		self.calculate_total_amounts()
		self.calculate_percentage_billed()
		self.set_status()


@frappe.whitelist()
def get_projectwise_timesheet_data(project=None, parent=None, from_time=None, to_time=None):
	condition = ""
	if project:
		condition += "AND tsd.project = %(project)s "
	if parent:
		condition += "AND tsd.parent = %(parent)s "
	if from_time and to_time:
		condition += "AND CAST(tsd.from_time as DATE) BETWEEN %(from_time)s AND %(to_time)s"

	query = f"""
		SELECT
			tsd.name as name,
			tsd.parent as time_sheet,
			tsd.from_time as from_time,
			tsd.to_time as to_time,
			tsd.billing_hours as billing_hours,
			tsd.billing_amount as billing_amount,
			tsd.activity_type as activity_type,
			tsd.description as description,
			ts.currency as currency,
			tsd.project_name as project_name
		FROM `tabTimesheet Detail` tsd
			INNER JOIN `tabTimesheet` ts
			ON ts.name = tsd.parent
		WHERE
			tsd.parenttype = 'Timesheet'
			AND tsd.docstatus = 1
			AND tsd.is_billable = 1
			AND tsd.sales_invoice is NULL
			{condition}
		ORDER BY tsd.from_time ASC
	"""

	filters = {"project": project, "parent": parent, "from_time": from_time, "to_time": to_time}

	return frappe.db.sql(query, filters, as_dict=1)


@frappe.whitelist()
def get_timesheet_detail_rate(timelog, currency):
	timelog_detail = frappe.db.sql(
		f"""SELECT tsd.billing_amount as billing_amount,
		ts.currency as currency FROM `tabTimesheet Detail` tsd
		INNER JOIN `tabTimesheet` ts ON ts.name=tsd.parent
		WHERE tsd.name = '{timelog}'""",
		as_dict=1,
	)[0]

	if timelog_detail.currency:
		exchange_rate = get_exchange_rate(timelog_detail.currency, currency)

		return timelog_detail.billing_amount * exchange_rate
	return timelog_detail.billing_amount


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_timesheet(doctype, txt, searchfield, start, page_len, filters):
	if not filters:
		filters = {}

	condition = ""
	if filters.get("project"):
		condition = "and tsd.project = %(project)s"

	return frappe.db.sql(
		f"""select distinct tsd.parent from `tabTimesheet Detail` tsd,
			`tabTimesheet` ts where
			ts.status in ('Submitted', 'Payslip') and tsd.parent = ts.name and
			tsd.docstatus = 1 and ts.total_billable_amount > 0
			and tsd.parent LIKE %(txt)s {condition}
			order by tsd.parent limit %(page_len)s offset %(start)s""",
		{
			"txt": "%" + txt + "%",
			"start": start,
			"page_len": page_len,
			"project": filters.get("project"),
		},
	)


@frappe.whitelist()
def get_timesheet_data(name, project):
	data = None
	if project and project != "":
		data = get_projectwise_timesheet_data(project, name)
	else:
		data = frappe.get_all(
			"Timesheet",
			fields=[
				"(total_billable_amount - total_billed_amount) as billing_amt",
				"total_billable_hours as billing_hours",
			],
			filters={"name": name},
		)
	return {
		"billing_hours": data[0].billing_hours if data else None,
		"billing_amount": data[0].billing_amt if data else None,
		"timesheet_detail": data[0].name if data and project and project != "" else None,
	}


@frappe.whitelist()
def make_sales_invoice(source_name, item_code=None, customer=None, currency=None):
	target = frappe.new_doc("Sales Invoice")
	timesheet = frappe.get_doc("Timesheet", source_name)

	if not timesheet.total_billable_hours:
		frappe.throw(_("Invoice can't be made for zero billing hour"))

	if timesheet.total_billable_hours == timesheet.total_billed_hours:
		frappe.throw(_("Invoice already created for all billing hours"))

	hours = flt(timesheet.total_billable_hours) - flt(timesheet.total_billed_hours)
	billing_amount = flt(timesheet.total_billable_amount) - flt(timesheet.total_billed_amount)
	billing_rate = billing_amount / hours

	target.company = timesheet.company
	target.project = timesheet.parent_project
	if customer:
		target.customer = customer
		default_price_list = frappe.get_value("Customer", customer, "default_price_list")
		if default_price_list:
			target.selling_price_list = default_price_list

	if currency:
		target.currency = currency

	if item_code:
		target.append("items", {"item_code": item_code, "qty": hours, "rate": billing_rate})

	for time_log in timesheet.time_logs:
		if time_log.is_billable:
			target.append(
				"timesheets",
				{
					"time_sheet": timesheet.name,
					"billing_hours": time_log.billing_hours,
					"billing_amount": time_log.billing_amount,
					"timesheet_detail": time_log.name,
					"activity_type": time_log.activity_type,
					"description": time_log.description,
				},
			)

	target.run_method("calculate_billing_amount_for_timesheet")
	target.run_method("set_missing_values")

	return target


@frappe.whitelist()
def get_activity_cost(employee=None, activity_type=None, currency=None):
	base_currency = frappe.defaults.get_global_default("currency")
	rate = frappe.db.get_values(
		"Activity Cost",
		{"employee": employee, "activity_type": activity_type},
		["costing_rate", "billing_rate"],
		as_dict=True,
	)
	if not rate:
		rate = frappe.db.get_values(
			"Activity Type",
			{"activity_type": activity_type},
			["costing_rate", "billing_rate"],
			as_dict=True,
		)
		if rate and currency and currency != base_currency:
			exchange_rate = get_exchange_rate(base_currency, currency)
			rate[0]["costing_rate"] = rate[0]["costing_rate"] * exchange_rate
			rate[0]["billing_rate"] = rate[0]["billing_rate"] * exchange_rate

	return rate[0] if rate else {}


@frappe.whitelist()
def get_events(start, end, filters=None):
	"""Returns events for Gantt / Calendar view rendering.
	:param start: Start date-time.
	:param end: End date-time.
	:param filters: Filters (JSON).
	"""
	filters = json.loads(filters)
	from frappe.desk.calendar import get_event_conditions

	conditions = get_event_conditions("Timesheet", filters)

	return frappe.db.sql(
		"""select `tabTimesheet Detail`.name as name,
			`tabTimesheet Detail`.docstatus as status, `tabTimesheet Detail`.parent as parent,
			from_time as start_date, hours, activity_type,
			`tabTimesheet Detail`.project, to_time as end_date,
			CONCAT(`tabTimesheet Detail`.parent, ' (', ROUND(hours,2),' hrs)') as title
		from `tabTimesheet Detail`, `tabTimesheet`
		where `tabTimesheet Detail`.parent = `tabTimesheet`.name
			and `tabTimesheet`.docstatus < 2
			and (from_time <= %(end)s and to_time >= %(start)s) {conditions} {match_cond}
		""".format(conditions=conditions, match_cond=get_match_cond("Timesheet")),
		{"start": start, "end": end},
		as_dict=True,
		update={"allDay": 0},
	)


def get_timesheets_list(doctype, txt, filters, limit_start, limit_page_length=20, order_by="creation"):
	user = frappe.session.user
	# find customer name from contact.
	customer = ""
	timesheets = []

	contact = frappe.db.exists("Contact", {"user": user})
	if contact:
		# find customer
		contact = frappe.get_doc("Contact", contact)
		customer = contact.get_link_for("Customer")

	if customer:
		sales_invoices = [
			d.name for d in frappe.get_all("Sales Invoice", filters={"customer": customer})
		] or [None]
		projects = [d.name for d in frappe.get_all("Project", filters={"customer": customer})]
		# Return timesheet related data to web portal.
		timesheets = frappe.db.sql(
			f"""
			SELECT
				ts.name, tsd.activity_type, ts.status, ts.total_billable_hours,
				COALESCE(ts.sales_invoice, tsd.sales_invoice) AS sales_invoice, tsd.project
			FROM `tabTimesheet` ts, `tabTimesheet Detail` tsd
			WHERE tsd.parent = ts.name AND
				(
					ts.sales_invoice IN %(sales_invoices)s OR
					tsd.sales_invoice IN %(sales_invoices)s OR
					tsd.project IN %(projects)s
				)
			ORDER BY `end_date` ASC
			LIMIT {limit_page_length} offset {limit_start}
		""",
			dict(sales_invoices=sales_invoices, projects=projects),
			as_dict=True,
		)  # nosec

	return timesheets


def get_list_context(context=None):
	return {
		"show_sidebar": True,
		"show_search": True,
		"no_breadcrumbs": True,
		"title": _("Timesheets"),
		"get_list": get_timesheets_list,
		"row_template": "templates/includes/timesheet/timesheet_row.html",
	}


def find_ts_for_time_log(values: dict) -> Timesheet | None:
	if values.get("parent"):
		return frappe.get_doc("Timesheet", values.parent)  # type: ignore

	ts_filters = {
		"employee": ["=", values.get("employee")],
		"docstatus": ["=", 0],
		"parent_project": ["=", values.get("project")],
	}

	if m := frappe.get_list("Timesheet", filters=ts_filters, fields=["name"], limit=1):
		return frappe.get_doc("Timesheet", m[0].name)  # type: ignore

	ts_filters["parent_project"] = ["is", "not set"]
	if m := frappe.get_list("Timesheet", filters=ts_filters, fields=["name"], limit=1):
		return frappe.get_doc("Timesheet", m[0].name)  # type: ignore


@frappe.whitelist()
def add_time_log(values: str | dict):
	values: dict = frappe.parse_json(values)  # type: ignore

	if (
		values.doctype == "Timesheet Detail"
		and values.name
		and frappe.db.exists("Timesheet Detail", values.name)
	):
		return update_time_log(values)

	# Find a suitable timesheet
	ts = find_ts_for_time_log(values)

	if not ts:
		ts = frappe.new_doc("Timesheet")  # type: ignore
		ts.update(
			{
				"employee": values.employee,
				"parent_project": None,
			}
		)

	# Insert timelog
	ts.append("time_logs", values)

	# Save timesheet
	ts.save()

	return ts.name


@frappe.whitelist()
def update_time_log(values: str | dict, delete=False):
	values = frappe.parse_json(values)
	try:
		tl = frappe.get_doc("Timesheet Detail", values.name)
		if tl.parenttype != "Timesheet":
			return
	except frappe.DoesNotExistError:
		if delete:
			return  # The timelog is already deleted, silently ignore
		else:
			values.name = None
			return add_time_log(values)

	ts: Timesheet = frappe.get_doc("Timesheet", tl.parent)  # type: ignore

	if not delete and not ts.is_new():
		# Check that the existing time log is compatible with the new values.
		# More precisely, check that the time log is still compatible with its parent.
		must_be_moved = False
		if values.project and ts.parent_project and (values.project != ts.parent_project):
			must_be_moved = True
		if values.employee and ts.employee and (values.employee != ts.employee):
			must_be_moved = True

		if must_be_moved:
			update_time_log(values, delete=True)
			values.name = None
			return add_time_log(values)

	# Update timelog
	if rows := ts.get("time_logs", {"name": values.name}):
		row = rows[0]
		if delete:
			ts.remove(row)
			if not ts.time_logs:
				ts.delete()
				return None
		else:
			row.update(values)
		ts.save()
		return ts.name


@frappe.whitelist()
def delete_time_log(values: str | dict):
	update_time_log(values, delete=True)


@frappe.whitelist()
def get_time_logs(start, end, filters=None):
	filters = json.loads(filters) if filters else []

	start_date = "ifnull(from_time, '0001-01-01 00:00:00')"
	end_date = "ifnull(to_time, '2999-12-31 00:00:00')"
	filters += [
		["Timesheet", "docstatus", "<", 2],
		["Timesheet Detail", start_date, "<=", end],
		["Timesheet Detail", end_date, ">=", start],
	]

	fields = {
		"time_logs.parent",
		"time_logs.docstatus",
		"time_logs.name",
		"time_logs.from_time",
		"time_logs.to_time",
		"time_logs.hours",
		"time_logs.activity_type",
		"time_logs.project",
		"time_logs.task",
		"time_logs.description",
		"employee",
	}

	meta = frappe.get_meta("Timesheet Detail")
	extra_dfs = []
	extra_dfs += meta.get("fields", {"reqd": ("=", 1)})
	extra_dfs += meta.get("fields", {"in_list_view": ("=", 1)})
	for df in extra_dfs:
		fields.add("time_logs." + df.fieldname)

	events = frappe.get_list("Timesheet", fields=fields, filters=filters, order_by="from_time desc")

	return events


@frappe.whitelist()
def get_timesheet_details(
	dt_start="",
	dt_end="",
	range: Literal[
		"",
		"last week",
		"last month",
		"last quarter",
		"last 6 months",
		"last year",
		"yesterday",
		"today",
		"tomorrow",
		"this week",
		"this month",
		"this quarter",
		"this year",
		"next week",
		"next month",
		"next quarter",
		"next 6 months",
		"next year",
	] = "",
	group_by: Literal["employee", "employee+project"] = "employee+project",
):
	import datetime
	from collections import defaultdict

	from frappe.query_builder.functions import Date, Sum
	from frappe.utils.data import get_timespan_date_range, getdate

	if not dt_start and not dt_end and range:
		if span := get_timespan_date_range(range):
			dt_start, dt_end = span

	dt_start = getdate(dt_start)
	assert isinstance(dt_start, datetime.date)

	dt_end = getdate(dt_end)
	assert isinstance(dt_end, datetime.date)

	if dt_start > dt_end:
		frappe.throw("dt_start > dt_end")

	employees: list[str] = frappe.get_list("Employee", pluck="name")

	TS = frappe.qb.DocType("Timesheet")
	TSD = frappe.qb.DocType("Timesheet Detail")

	sum_hours = Sum(TSD.hours).as_("sum_hours")
	date_start = Date(TSD.from_time).as_("date_start")
	project = TSD.project

	grouping_key = (date_start,)
	if group_by == "employee+project":
		grouping_key = (date_start, TS.employee, project)
	elif group_by == "employee":
		grouping_key = (date_start, TS.employee)

	q = (
		frappe.qb.from_(TSD)
		.join(TS)
		.on((TS.name == TSD.parent) & ("Timesheet" == TSD.parenttype) & ("time_logs" == TSD.parentfield))
		.select(TS.employee, project, date_start, sum_hours)
		.where((date_start >= dt_start) & (date_start <= dt_end))
		.groupby(*grouping_key)
		.where(TS.employee.isin(employees))
		.orderby(*grouping_key)
	)

	out = defaultdict(lambda: [])

	date = dt_start
	while date <= dt_end:
		out[str(date)] = []
		date = add_to_date(date, days=1)

	for row in q.run(as_dict=True):
		out[str(row.date_start)].append(row)

	return out
