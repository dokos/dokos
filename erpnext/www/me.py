# Copyright (c) 2019, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

import frappe
from payments.www.me import get_context as get_payments_context

no_cache = 1


def get_context(context):
	get_payments_context(context)

	context.current_user = frappe.get_doc("User", frappe.session.user)
