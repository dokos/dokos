# Copyright (c) 2018, Frappe Technologies and contributors
# For license information, please see license.txt

from itertools import groupby

import frappe

from erpnext.accounts.utils import get_fiscal_year
from erpnext.regional.report.fichier_des_ecritures_comptables.fichier_des_ecritures_comptables import (
	export_report,
)


# don't remove this function it is used in tests
def test_method():
	"""test function"""
	return "overridden"


def on_closing_voucher_submission(doc, method):
	fiscal_year = get_fiscal_year(fiscal_year=doc.fiscal_year, as_dict=True)
	generate_ecriturenum(doc, fiscal_year)
	generate_fec_report(doc, fiscal_year)


def generate_ecriturenum(doc, fiscal_year):
	gl_entries = frappe.get_all(
		"GL Entry",
		filters={
			"fiscal_year": doc.fiscal_year,
			"posting_date": ("between", [fiscal_year.year_start_date, doc.transaction_date]),
		},
		fields=["name", "accounting_entry_number", "posting_date", "accounting_journal"],
		order_by="accounting_journal, posting_date ASC",
	)

	entries_by_journal = groupby(
		sorted(gl_entries, key=lambda x: x.accounting_journal or ""),
		lambda x: x.accounting_journal,
	)
	for _, values in entries_by_journal:
		values = sorted(values, key=lambda x: x.accounting_entry_number)
		for index, folio in enumerate(groupby(values, lambda x: x.accounting_entry_number)):
			for entry in folio[1]:
				frappe.db.set_value("GL Entry", entry.name, "ecriturenum", index + 1)


def generate_fec_report(doc, fiscal_year):
	if frappe.db.get_value("Company", doc.company, "country") == "France":
		filters = {
			"company": doc.company,
			"fiscal_year": doc.fiscal_year,
			"from_date": fiscal_year.year_start_date,
			"to_date": doc.transaction_date,
		}
		fec_file, title = export_report(filters, return_file=True)

		_file = frappe.get_doc(
			{
				"doctype": "File",
				"file_name": f"{title}.txt",
				"is_private": True,
				"content": fec_file,
				"attached_to_name": doc.name,
				"attached_to_doctype": doc.doctype,
			}
		)
		return _file.insert(ignore_if_duplicate=True)
