from frappe.utils import add_months, cint, date_diff, flt, get_last_day, getdate

from erpnext.assets.doctype.asset_depreciation_schedule.asset_depreciation_schedule import (
	get_straight_line_or_manual_depr_amount,
)


def get_depreciation_amount(
	asset_depr_schedule,
	asset,
	depreciable_value,
	yearly_opening_wdv,
	fb_row,
	schedule_idx=0,
	prev_depreciation_amount=0,
	has_wdv_or_dd_non_yearly_pro_rata=False,
	number_of_pending_depreciations=0,
	prev_per_day_depr=0,
):
	if fb_row.depreciation_method in ("Straight Line", "Manual"):
		# if the Depreciation Schedule is being prepared for the first time
		if not asset.flags.increase_in_asset_life:
			depreciation_amount = (
				flt(asset.gross_purchase_amount) - flt(fb_row.expected_value_after_useful_life)
			) / flt(fb_row.total_number_of_depreciations)

			return depreciation_amount, None

	return get_straight_line_or_manual_depr_amount(
		asset_depr_schedule, asset, fb_row, schedule_idx, number_of_pending_depreciations
	), None


def get_total_days(date, frequency):
	period_start_date = add_months(date, cint(frequency) * -1)
	if is_last_day_of_the_month(date):
		period_start_date = get_last_day(period_start_date)

	return min(date_diff(date, period_start_date), 360)


def is_last_day_of_the_month(date):
	last_day_of_the_month = get_last_day(date)

	return getdate(last_day_of_the_month) == getdate(date)


def date_difference(to_date, from_date):
	"""
	Calculate a difference based on a 30 days per months rule
	"""
	day = getdate(from_date).day
	month = getdate(from_date).month

	if (month == 2 and day >= 28) or day > 30:
		day = 30

	first_month_diff = 30 - day

	return 30 * (getdate(to_date).month - month) + first_month_diff
