import frappe
from frappe import _
from frappe.contacts.doctype.address.address import get_default_address
from frappe.utils import cint, date_diff, nowdate

from erpnext.regional.france.pappers.entreprise import PappersEntreprise
from erpnext.regional.france.pappers.recherche import PappersRecherche


def validate(doc, method):
	get_pappers_data(doc)

def get_pappers_data(doc):
	meta = frappe.get_meta(doc.doctype)
	if meta.has_field("siren_number"):
		if doc.tax_id and not doc.get("siren_number"):
			get_siren_from_tax_id(doc.tax_id)
		get_info_from_pappers(doc)


def get_siren_from_tax_id(tax_id):
	return tax_id[4:]


def get_info_from_pappers(doc):
	global_defauts = frappe.get_single("Global Defaults")
	if not global_defauts.enable_pappers or not global_defauts.pappers_api_key:
		return

	if not doc.siren_number or not (doc.tax_id and doc.tax_id.startswith("FR")):
		return

	if date_diff(nowdate(), doc.last_pappers_update) > cint(global_defauts.pappers_update_interval):
		return

	data = PappersEntreprise().get({"siren": doc.siren_number or get_siren_from_tax_id(doc.tax_id)})

	if data and not data.get("statusCode"):
		update_tax_id(doc, data.get("numero_tva_intracommunautaire"))

		meta = frappe.get_meta(doc.doctype)
		for key, value in data.items():
			if meta.has_field(key):
				doc.set(key, value)

		update_billing_address(doc, data)

		doc.last_pappers_update = nowdate()


def update_tax_id(doc, vat_number):
	if not doc.tax_id:
		doc.tax_id = vat_number

	elif doc.tax_id != vat_number:
		frappe.msgprint(
			_(
				"The VAT number registered in this document doesn't match the VAT number available publicly for this company: {}".format(  # noqa: UP032
					vat_number
				)
			)
		)


def update_billing_address(doc, pappers_data):
	data = frappe._dict(pappers_data.get("siege", {}))
	if not get_default_address(doc.doctype, doc.name, sort_key="is_primary_address"):
		if data.get("ville") and frappe.db.exists("Country", data.get("pays")):
			address = frappe.new_doc("Address")
			address.update(
				{
					"address_title": pappers_data.get("denomination"),
					"address_type": "Billing",
					"address_line1": data.get("adresse_ligne_1"),
					"address_line2": data.get("adresse_ligne_2"),
					"city": data.get("ville"),
					"country": data.get("pays"),
				}
			)
			address.append("links", {"link_doctype": doc.doctype, "link_name": doc.name})
			address.insert()


@frappe.whitelist()
def company_query(txt):
	if txt:
		if txt.startswith("FR") and len(txt) == 13 and txt[4:].isdigit():
			if res := get_company_by_siren(get_siren_from_tax_id(txt)):
				return res
		if len(txt.replace(" ", "")) == 9 and txt.replace(" ", "").isdigit():
			if res := get_company_by_siren(txt.replace(" ", "")):
				return res

		res = PappersRecherche().get(
			{"q": txt, "cibles": "nom_entreprise,siren,siret", "longueur": 100, "api_token": None}
		)

		if res.get("statusCode") == 401:
			# TODO: Handle when all tokens have been used
			return []

		if res:
			return list(
				{
					"label": r.get("nom_entreprise"),
					"value": r.get("numero_tva_intracommunautaire") or calculated_vat_number(r.get('siren')),
					"description": f"SIREN: {r.get('siren_formate')}<br>{r.get('siege', {}).get('adresse_ligne_1', '')} {r.get('siege', {}).get('code_postal', '')} {r.get('siege', {}).get('ville', '')}",
				}
				for r in res.get("resultats_nom_entreprise", [])
			)

	return []

def get_company_by_siren(siren):
	res = PappersEntreprise().get({"siren": siren})
	if not res.get("statusCode"):
		return list([
			{
				"label": res.get("nom_entreprise"),
				"value": res.get("numero_tva_intracommunautaire"),
				"description": f"SIREN: {res.get('siren_formate')}<br>{res.get('siege', {}).get('adresse_ligne_1', '')} {res.get('siege', {}).get('code_postal', '')} {res.get('siege', {}).get('ville', '')}",
			}
		])

	return []

def calculated_vat_number(siren):
	return f"FR{(12+3*int(siren)%97)%97}{siren}"
