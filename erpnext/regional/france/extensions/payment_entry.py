import frappe
from frappe import _
from frappe.utils import flt


def add_regional_taxes(doc):
	if frappe.db.get_value("Company", doc.company, "country") != "France":
		return

	company = frappe.get_cached_doc("Company", doc.company)
	if not company.suspense_accounts_settings:
		return

	supense_accounts_mapping = {
		account.vat_suspense_account: account.vat_control_account
		for account in company.suspense_accounts_settings
	}
	supense_accounts = supense_accounts_mapping.keys()

	doc.set("taxes", [])
	for reference in doc.references:
		res = get_tax_reversal_entries(
			doc.company, reference.reference_doctype, reference.reference_name, supense_accounts
		)
		if len(res) == 1:
			amount = 0.0
			append_taxes(doc, res[0], supense_accounts_mapping, amount)
		else:
			for row in res:
				amount = abs(row.get("total"))
				append_taxes(doc, row, supense_accounts_mapping, amount)


def append_taxes(doc, row, supense_accounts_mapping, amount=None):
	tax_rate = frappe.get_cached_value("Account", row["account"], "tax_rate")
	tax_amount = min(amount, (doc.get("received_amount") * flt(tax_rate) / 100.0))

	doc.append(
		"taxes",
		{
			"add_deduct_tax": "Deduct",
			"included_in_paid_amount": 1 if not amount else 0,
			"charge_type": "On Paid Amount" if not amount else "Actual",
			"account_head": row["account"],
			"rate": tax_rate,
			"tax_amount": tax_amount,
			"description": row["remarks"],
		},
	)

	doc.append(
		"taxes",
		{
			"add_deduct_tax": "Add",
			"included_in_paid_amount": 1 if not amount else 0,
			"charge_type": "On Paid Amount" if not amount else "Actual",
			"account_head": supense_accounts_mapping.get(row.account),
			"rate": tax_rate,
			"tax_amount": tax_amount,
			"description": row["remarks"],
		},
	)


def get_tax_reversal_entries(company, voucher_type, voucher_no, pending_vat_accounts):
	filters = {
		"is_cancelled": 0,
		"account": ["in", pending_vat_accounts],
		"voucher_type": voucher_type,
		"voucher_no": voucher_no,
		"company": company,
	}

	return frappe.db.get_all(
		"GL Entry",
		filters=filters,
		fields=["account", "sum(credit-debit) as total", "remarks"],
		group_by="account",
	)
