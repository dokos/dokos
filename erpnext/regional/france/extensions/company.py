import frappe
from frappe import _
from frappe.utils import flt

from erpnext.regional.france.pappers.document import PappersDocument


def regional_validation(doc):
	for row in doc.suspense_accounts_settings:
		if row.vat_suspense_account == row.vat_control_account:
			frappe.throw(_("Please set different account for VAT Suspense Account and VAT Control Account"))

		for field in ("vat_suspense_account", "vat_control_account"):
			if flt(frappe.db.get_value("Account", field, "tax_rate")) != flt(row.tax_rate):
				frappe.db.set_value("Account", field, "tax_rate", flt(row.tax_rate))

	if not doc.suspense_accounts_settings:
		for acc_numbers in ("44574%", "44564%"):
			for account in frappe.get_all("Account", filters=dict(account_number=("like", acc_numbers), account_type="Tax", company=doc.name, is_group=0), fields=["name", "tax_rate"]):
				if control_account := frappe.db.get_value("Account", dict(
					account_number=("like", f"{acc_numbers[:4]}%"),
					name=("!=", account.name),
					tax_rate=account.tax_rate,
					account_type="Tax",
					company=doc.name
					),
				):
					doc.append("suspense_accounts_settings", {
						"vat_suspense_account": account.name,
						"vat_control_account": control_account,
						"tax_rate": account.tax_rate
					})


@frappe.whitelist()
def get_extrait_pappers(siren):
	response = PappersDocument().get_extrait_pappers(siren)

	if response.status_code == 401:
		frappe.redirect_to_message(
			_("Document Unavailable"),
			_(
				"You have reached your API requests limit for this month.<br>Please buy additional tokens at pappers.fr"
			),
		)
		frappe.local.flags.redirect_location = frappe.local.response.location
	else:
		frappe.local.response.filename = f"{siren}.pdf"
		frappe.local.response.filecontent = response.content
		frappe.local.response.type = "pdf"


@frappe.whitelist()
def get_extrait_inpi(siren):
	response = PappersDocument().get_extrait_inpi(siren)

	if response.status_code == 401:
		frappe.redirect_to_message(
			_("Document Unavailable"),
			_(
				"You have reached your API requests limit for this month.<br>Please buy additional tokens at pappers.fr"
			),
		)
		frappe.local.flags.redirect_location = frappe.local.response.location
	else:
		frappe.local.response.filename = f"{siren}.pdf"
		frappe.local.response.filecontent = response.content
		frappe.local.response.type = "pdf"


@frappe.whitelist()
def get_extrait_insee(siren):
	response = PappersDocument().get_extrait_insee(siren)

	if response.status_code == 401:
		frappe.redirect_to_message(
			_("Document Unavailable"),
			_(
				"You have reached your API requests limit for this month.<br>Please buy additional tokens at pappers.fr"
			),
		)
		frappe.local.flags.redirect_location = frappe.local.response.location
	else:
		frappe.local.response.filename = f"{siren}.pdf"
		frappe.local.response.filecontent = response.content
		frappe.local.response.type = "pdf"
