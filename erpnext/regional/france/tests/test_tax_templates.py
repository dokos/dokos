import frappe
from frappe.tests import IntegrationTestCase

from erpnext.regional.france.tests.test_assets import create_company


class TestTaxTemplatesForFrance(IntegrationTestCase):
	@classmethod
	def setUpClass(cls):
		super().setUpClass()
		company = "_Test French Company 2"
		create_company(
			company_name=company, country="France", currency="EUR", chart_of_accounts="Plan Comptable Général"
		)
		frappe.flags.country = "France"

	@classmethod
	def tearDownClass(cls):
		frappe.flags.country = None

	def test_france_tax_templates(self):
		self.assertListEqual(
			frappe.get_all(
				"Sales Taxes and Charges Template", {"company": "_Test French Company 2"}, pluck="name"
			),
			[
				"TVA 20% Collectée - _FC2",
				"TVA collectée sur encaissements - 20% - _FC2",
				"TVA 10% Collectée - _FC2",
				"TVA 5.5% Collectée - _FC2",
				"TVA 2.1% Collectée - _FC2",
			],
		)
		self.assertSequenceSubset(
			frappe.get_all("Account", {"company": "_Test French Company 2"}, pluck="name"),
			[
				"201 - Frais d'établissement - _FC2",
				"320 - Autres approvisionnements - _FC2",
				"411 - Clients - _FC2",
				"486 - Charges constatées d'avance - _FC2",
			],
		)
