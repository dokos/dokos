import frappe
from frappe.tests import IntegrationTestCase
from frappe.utils import nowdate

from erpnext.accounts.doctype.payment_entry.test_payment_entry import get_payment_entry
from erpnext.accounts.doctype.purchase_invoice.test_purchase_invoice import make_purchase_invoice
from erpnext.accounts.doctype.sales_invoice.test_sales_invoice import create_sales_invoice
from erpnext.regional.france.tests.test_assets import create_company


class TestVATForFrance(IntegrationTestCase):
	@classmethod
	def setUpClass(cls):
		super().setUpClass()
		company = "_Test French Company 3"
		cls.company = create_company(
			company_name=company, country="France", currency="EUR", chart_of_accounts="Plan Comptable Général"
		)
		frappe.flags.country = "France"

		cls.debit_account = frappe.new_doc("Account")
		cls.debit_account.update(
			{
				"account_name": "_Test Receivable EUR",
				"parent_account": "C - Créances" + " - " + frappe.db.get_value("Company", company, "abbr"),
				"company": company,
				"is_group": 0,
				"account_type": "Receivable",
				"account_currency": "EUR",
			}
		)
		cls.debit_account.insert(ignore_if_duplicate=True)

		cls.credit_account = frappe.new_doc("Account")
		cls.credit_account.update(
			{
				"account_name": "_Test Payables EUR",
				"parent_account": "F - Dettes fournisseurs et comptes rattachés - Liability"
				+ " - "
				+ frappe.db.get_value("Company", company, "abbr"),
				"company": company,
				"is_group": 0,
				"account_type": "Payable",
				"account_currency": "EUR",
			}
		)
		cls.credit_account.insert(ignore_if_duplicate=True)

		cls.customer = frappe.new_doc("Customer")
		cls.customer.update(
			{
				"customer_group": "_Test Customer Group",
				"customer_name": "_Test Customer EUR",
				"customer_type": "Individual",
				"territory": "_Test Territory",
				"default_currency": "EUR",
			}
		)
		cls.customer.insert(ignore_if_duplicate=True)

		cls.supplier = frappe.new_doc("Supplier")
		cls.supplier.update(
			{
				"supplier_group": "_Test Supplier Group",
				"supplier_name": "_Test Supplier EUR",
				"territory": "_Test Territory",
				"default_currency": "EUR",
			}
		)
		cls.supplier.insert(ignore_if_duplicate=True)

		if tax_template := frappe.db.get_value(
			"Sales Taxes and Charges Template", dict(company=company, is_default=True)
		):
			frappe.db.set_value("Sales Taxes and Charges Template", tax_template, "is_default", False)

	@classmethod
	def tearDownClass(cls):
		frappe.flags.country = None

	def test_accounts_are_created(self):
		all_accounts = frappe.get_all(
			"Account", filters={"account_number": ("is", "set")}, pluck="account_number"
		)
		self.assertIn("445742", all_accounts)
		self.assertIn("445642", all_accounts)

		company = frappe.get_doc("Company", TestVATForFrance.company)
		company.suspense_accounts_settings = []
		company.save()
		for row in company.suspense_accounts_settings:
			self.assertEqual(row.vat_suspense_account[:4], row.vat_control_account[:4])

	@IntegrationTestCase.change_settings("Accounts Settings", {"mandatory_accounting_journal": 0})
	def test_vat_on_sales_payments(self):
		company = frappe.get_doc("Company", TestVATForFrance.company)
		company.suspense_accounts_settings = []
		company.save()
		company_abbr = company.abbr

		si = create_sales_invoice(
			company=TestVATForFrance.company,
			currency="EUR",
			customer=TestVATForFrance.customer.name,
			debit_to=TestVATForFrance.debit_account.name,
			cost_center=f"Main - {company_abbr}",
			income_account=f"701 - Ventes de produits finis - {company_abbr}",
			do_not_save=1,
		)
		si.taxes_and_charges = f"TVA collectée sur encaissements - 20% - {company_abbr}"
		si.set_missing_values()
		si.set_taxes()
		si.calculate_taxes_and_totals()
		si.insert()
		si.submit()

		gl_entries = frappe.get_all(
			"GL Entry", filters={"voucher_type": si.doctype, "voucher_no": si.name}, pluck="account"
		)

		self.assertIn(f"445742 - TVA collectée sur encaissements - 20% - {company_abbr}", gl_entries)

		pe = get_payment_entry("Sales Invoice", si.name)
		pe.reference_no = "1"
		pe.reference_date = nowdate()
		pe.insert()
		pe.submit()

		gl_entries = frappe.get_all(
			"GL Entry",
			filters={"voucher_type": pe.doctype, "voucher_no": pe.name},
			fields=["account", "debit", "credit"],
		)
		accounts = [gl.account for gl in gl_entries]
		self.assertIn(f"445742 - TVA collectée sur encaissements - 20% - {company_abbr}", accounts)
		self.assertIn(f"445720 - TVA 20% Collectée - {company_abbr}", accounts)

		tax_map = {
			f"445742 - TVA collectée sur encaissements - 20% - {company_abbr}": "debit",
			f"445720 - TVA 20% Collectée - {company_abbr}": "credit",
		}

		result = []
		for gl_entry in gl_entries:
			if field := tax_map.get(gl_entry.account):
				result.append(gl_entry.get(field))

		self.assertTrue(len(result), 2)
		self.assertEqual(result[0], result[1])

	@IntegrationTestCase.change_settings("Accounts Settings", {"mandatory_accounting_journal": 0})
	def test_multiple_vat_on_sales_payments(self):
		company = frappe.get_doc("Company", TestVATForFrance.company)

		create_sales_tax_templates(company)
		create_test_items(company)
		company.suspense_accounts_settings = []
		company.save()

		si = create_sales_invoice(
			company=TestVATForFrance.company,
			currency="EUR",
			customer=TestVATForFrance.customer.name,
			debit_to=TestVATForFrance.debit_account.name,
			cost_center=f"Main - {company.abbr}",
			income_account=f"701 - Ventes de produits finis - {company.abbr}",
			do_not_save=1,
		)
		si.set("items", [])
		for item in ["VATONPAYMENTS_10", "VATONPAYMENTS_20"]:
			si.append("items", {"item_code": item, "rate": 100})

		si.set_missing_values()
		si.set_taxes()
		si.calculate_taxes_and_totals()
		si.insert()
		si.submit()

		gl_entries = frappe.get_all(
			"GL Entry", filters={"voucher_type": si.doctype, "voucher_no": si.name}, pluck="account"
		)
		si.reload()

		self.assertIn(f"445742 - TVA collectée sur encaissements - 20% - {company.abbr}", gl_entries)
		self.assertIn(f"445741 - TVA collectée sur encaissements - 10% - {company.abbr}", gl_entries)

		pe = get_payment_entry("Sales Invoice", si.name)
		pe.reference_no = "1"
		pe.reference_date = nowdate()
		pe.insert()
		pe.submit()

		gl_entries = frappe.get_all(
			"GL Entry",
			filters={"voucher_type": pe.doctype, "voucher_no": pe.name},
			fields=["account", "debit", "credit"],
		)
		accounts = [gl.account for gl in gl_entries]
		self.assertIn(f"445742 - TVA collectée sur encaissements - 20% - {company.abbr}", accounts)
		self.assertIn(f"445720 - TVA 20% Collectée - {company.abbr}", accounts)
		self.assertIn(f"445741 - TVA collectée sur encaissements - 10% - {company.abbr}", accounts)
		self.assertIn(f"445710 - TVA 10% Collectée - {company.abbr}", accounts)

		tax_map = {
			f"445742 - TVA collectée sur encaissements - 20% - {company.abbr}": "debit",
			f"445720 - TVA 20% Collectée - {company.abbr}": "credit",
			f"445741 - TVA collectée sur encaissements - 10% - {company.abbr}": "debit",
			f"445710 - TVA 10% Collectée - {company.abbr}": "credit",
		}

		result = []
		for gl_entry in gl_entries:
			if field := tax_map.get(gl_entry.account):
				result.append(gl_entry.get(field))

		self.assertTrue(len(result), 4)
		self.assertEqual(result[0], result[1])
		self.assertEqual(result[2], result[3])

	@IntegrationTestCase.change_settings("Accounts Settings", {"mandatory_accounting_journal": 0})
	def test_vat_on_purchase_payments(self):
		company = frappe.get_doc("Company", TestVATForFrance.company)

		create_purchase_tax_templates(company)

		company.suspense_accounts_settings = []
		company.save()
		company_abbr = company.abbr

		pi = make_purchase_invoice(
			company=TestVATForFrance.company,
			currency="EUR",
			supplier=TestVATForFrance.supplier.name,
			credit_to=TestVATForFrance.credit_account.name,
			cost_center=f"Main - {company_abbr}",
			warehouse=frappe.db.get_value("Warehouse", dict(is_group=0, company=company.name)),
			expense_account=f"600 - Achats - {company_abbr}",
			do_not_save=1,
		)
		pi.taxes_and_charges = f"TVA déductible sur encaissements - 20% - {company_abbr}"
		pi.set_missing_values()
		pi.set_taxes()
		pi.calculate_taxes_and_totals()
		pi.insert()
		pi.submit()

		gl_entries = frappe.get_all(
			"GL Entry", filters={"voucher_type": pi.doctype, "voucher_no": pi.name}, pluck="account"
		)

		self.assertIn(f"445642 - TVA déductible sur encaissements - 20% - {company_abbr}", gl_entries)

		pe = get_payment_entry("Purchase Invoice", pi.name)
		pe.reference_no = "1"
		pe.reference_date = nowdate()
		pe.insert()
		pe.submit()

		gl_entries = frappe.get_all(
			"GL Entry",
			filters={"voucher_type": pe.doctype, "voucher_no": pe.name},
			fields=["account", "debit", "credit"],
		)
		accounts = [gl.account for gl in gl_entries]
		self.assertIn(f"445642 - TVA déductible sur encaissements - 20% - {company_abbr}", accounts)
		self.assertIn(
			f"445662 - TVA déductible sur acquisition intracommunautaires - {company_abbr}", accounts
		)

		tax_map = {
			f"445642 - TVA déductible sur encaissements - 20% - {company_abbr}": "credit",
			f"445662 - TVA déductible sur acquisition intracommunautaires - {company_abbr}": "debit",
		}

		result = []
		for gl_entry in gl_entries:
			if field := tax_map.get(gl_entry.account):
				result.append(gl_entry.get(field))

		self.assertTrue(len(result), 2)
		self.assertEqual(result[0], result[1])


def create_sales_tax_templates(company):
	if not frappe.db.exists("Account", f"445741 - TVA collectée sur encaissements - 10% - {company.abbr}"):
		account = frappe.new_doc("Account")
		account.account_number = 445741
		account.account_name = "TVA collectée sur encaissements - 10%"
		account.company = company.name
		account.account_type = "Tax"
		account.parent_account = (
			f"4457 - Taxes sur le chiffre d'affaires collectées par l'entreprise - {company.abbr}"
		)
		account.tax_rate = 10.0
		account.insert(ignore_if_duplicate=True)

	if not frappe.db.exists("Item Tax Template", f"TVA collectée sur encaissements - 10% - {company.abbr}"):
		tax_template = frappe.new_doc("Item Tax Template")
		tax_template.title = "TVA collectée sur encaissements - 10%"
		tax_template.company = company.name
		tax_template.applicable_for = "Sales"
		tax_template.append(
			"taxes",
			{
				"tax_type": frappe.db.get_value("Account", dict(account_number=445741, company=company.name)),
				"tax_rate": 10.0,
				"description": "TVA 10%",
			},
		)
		tax_template.insert(ignore_if_duplicate=True)


def create_purchase_tax_templates(company):
	if not frappe.db.exists("Account", f"445642 - TVA déductible sur encaissements - 20% - {company.abbr}"):
		account = frappe.new_doc("Account")
		account.account_number = 445642
		account.account_name = "TVA déductible sur encaissements -20%"
		account.company = company.name
		account.account_type = "Tax"
		account.parent_account = f"4456 - Taxes sur le chiffre d'affaires déductibles - {company.abbr}"
		account.tax_rate = 20.0
		account.insert(ignore_if_duplicate=True)

	if not frappe.db.exists("Item Tax Template", f"TVA déductible sur encaissements - 20% - {company.abbr}"):
		tax_template = frappe.new_doc("Item Tax Template")
		tax_template.title = "TVA déductible sur encaissements - 20%"
		tax_template.company = company.name
		tax_template.applicable_for = "Purchases"
		tax_template.append(
			"taxes",
			{
				"tax_type": frappe.db.get_value("Account", dict(account_number=445642, company=company.name)),
				"tax_rate": 20.0,
				"description": "TVA 20%",
			},
		)
		tax_template.insert(ignore_if_duplicate=True)


def create_test_items(company):
	for item in ["VATONPAYMENTS_10", "VATONPAYMENTS_20"]:
		doc = frappe.new_doc("Item")
		doc.item_code = item
		doc.item_group = "Services"
		doc.stock_uom = "_Test UOM"
		doc.append(
			"taxes",
			{
				"company": company.name,
				"item_tax_template": f"TVA collectée sur encaissements - {item[-2:]}% - {company.abbr}",
			},
		)
		doc.insert(ignore_if_duplicate=True)
