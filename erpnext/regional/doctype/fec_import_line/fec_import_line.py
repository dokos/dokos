# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

from frappe.model.document import Document


class FECImportLine(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		account: DF.Link | None
		accounting_journal: DF.Link | None
		compauxlib: DF.SmallText | None
		compauxnum: DF.Data | None
		comptelib: DF.SmallText | None
		comptenum: DF.Data | None
		credit: DF.Float
		datelet: DF.Data | None
		debit: DF.Float
		ecrituredate: DF.Data | None
		ecriturelet: DF.Data | None
		ecriturelib: DF.SmallText | None
		ecriturenum: DF.Data | None
		hashed_data: DF.Data | None
		idevise: DF.Data | None
		journalcode: DF.Data | None
		journallib: DF.SmallText | None
		montantdevise: DF.Float
		parent: DF.Data
		parentfield: DF.Data
		parenttype: DF.Data
		party: DF.DynamicLink | None
		party_type: DF.Link | None
		piecedate: DF.Data | None
		pieceref: DF.Data | None
		posting_date: DF.Date | None
		reconciliation_date: DF.Date | None
		transaction_date: DF.Date | None
		validation_date: DF.Date | None
		validdate: DF.Data | None
	# end: auto-generated types

	pass
