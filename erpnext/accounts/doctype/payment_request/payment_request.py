# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

import json
import warnings

import frappe
from frappe import _, qb
from frappe.model.document import Document
from frappe.query_builder.functions import Abs, Sum
from frappe.utils import flt, get_url, nowdate
from frappe.utils.background_jobs import enqueue
from payments.utils.utils import can_make_immediate_payment

from erpnext import get_company_currency
from erpnext.accounts.doctype.accounting_dimension.accounting_dimension import (
	get_accounting_dimensions,
)
from erpnext.accounts.doctype.payment_entry.payment_entry import get_company_defaults, get_payment_entry
from erpnext.accounts.party import get_party_account, get_party_bank_account
from erpnext.accounts.utils import get_account_currency, get_currency_precision
from erpnext.utilities import payment_app_import_guard, webshop_app_import_guard

ALLOWED_DOCTYPES_FOR_PAYMENT_REQUEST = [
	"Sales Order",
	"Purchase Order",
	"Sales Invoice",
	"Purchase Invoice",
	"POS Invoice",
	"Fees",
]


def _get_payment_gateway_controller(*args, **kwargs):
	with payment_app_import_guard():
		from payments.utils import get_payment_gateway_controller

	return get_payment_gateway_controller(*args, **kwargs)


def _get_shopping_cart_settings():
	with webshop_app_import_guard():
		from webshop.webshop.shopping_cart.cart import get_shopping_cart_settings

	return get_shopping_cart_settings()


class PaymentRequest(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		account: DF.Link | None
		amended_from: DF.Link | None
		bank_account: DF.Link | None
		base_amount: DF.Currency
		company: DF.Link | None
		cost_center: DF.Link | None
		currency: DF.Link | None
		email_template: DF.Link | None
		email_to: DF.Data | None
		exchange_rate: DF.Float
		failed_reason: DF.Data | None
		fee_amount: DF.Currency
		grand_total: DF.Currency
		make_sales_invoice: DF.Check
		message: DF.TextEditor | None
		mode_of_payment: DF.Link | None
		mute_email: DF.Check
		naming_series: DF.Literal["ACC-PRQ-.YYYY.-.MM.-.DD.-"]
		outstanding_amount: DF.Currency
		party: DF.DynamicLink | None
		party_account_currency: DF.Link | None
		party_name: DF.Data | None
		party_type: DF.Link | None
		payment_channel: DF.Literal["Email", "Phone", "Other"]
		payment_gateway: DF.Link | None
		payment_gateway_account: DF.Link | None
		payment_key: DF.Data | None
		payment_request_type: DF.Literal["Outward", "Inward"]
		payment_url: DF.Data | None
		phone_number: DF.Data | None
		print_format: DF.Literal[None]
		project: DF.Link | None
		reference_doctype: DF.Link | None
		reference_name: DF.DynamicLink | None
		status: DF.Literal[
			"", "Draft", "Requested", "Initiated", "Pending", "Paid", "Failed", "Cancelled", "Completed"
		]
		subject: DF.Data | None
		subscription: DF.Link | None
		transaction_date: DF.Date | None
		transaction_reference: DF.Data | None
	# end: auto-generated types

	def before_insert(self):
		self.payment_key = None
		if self.payment_request_type == "Inward":
			self.generate_payment_key()

	def validate(self):
		if self.get("__islocal"):
			self.status = "Draft"
		self.set_payment_request_type()
		self.validate_reference_document()
		self.validate_payment_request_amount()
		if self.payment_request_type == "Inward":
			self.set_customer()
			if self.get("payment_channel") != "Phone":  # @dokos
				self.set_payment_url()
			self.set_payment_gateway_from_mode_of_payment()

		if self.payment_request_type == "Outward":
			self.set_supplier()

		if self.email_template and not self.message:
			self.set_message_from_template()

		self.get_subscription_reference()  # @dokos

	def set_payment_request_type(self):
		self.payment_request_type = "Inward"
		if self.reference_doctype in [
			*frappe.get_hooks("advance_payment_payable_doctypes"),
			"Purchase Invoice",
		]:  # @dokos
			self.payment_request_type = "Outward"
			self.mute_email = True

	def validate_reference_document(self):
		if not self.reference_doctype or not self.reference_name:
			frappe.throw(_("To create a Payment Request reference document is required"))

	def validate_payment_request_amount(self):
		if self.grand_total == 0:
			frappe.throw(
				_("{0} cannot be zero").format(self.get_label_from_fieldname("grand_total")),
				title=_("Invalid Amount"),
			)

		existing_payment_request_amount = flt(
			get_existing_payment_request_amount(self.reference_doctype, self.reference_name)
		)

		ref_doc = frappe.get_doc(self.reference_doctype, self.reference_name)
		if not hasattr(ref_doc, "order_type") or ref_doc.order_type != "Shopping Cart":
			ref_amount = get_amount(ref_doc)
			if not ref_amount:
				frappe.throw(_("Payment Entry is already created"))

			if existing_payment_request_amount + flt(self.grand_total) > ref_amount:
				frappe.throw(
					_("Total Payment Request amount cannot be greater than {0} amount").format(
						self.reference_doctype
					)
				)

	def set_gateway_account(self):
		accounts = frappe.get_all(
			"Payment Gateway Account",
			filters={"payment_gateway": self.payment_gateway, "currency": self.currency},
			fields=["name", "is_default", "payment_channel"],
		)

		default_accounts = [x for x in accounts if x["is_default"]]
		if default_accounts:
			self.db_set("payment_gateway_account", default_accounts[0]["name"])
			self.db_set("payment_channel", default_accounts[0]["payment_channel"])
		elif accounts:
			self.db_set("payment_gateway_account", accounts[0]["name"])
			self.db_set("payment_channel", accounts[0]["payment_channel"])

	def get_payment_account(self):
		if self.payment_gateway_account:
			return frappe.db.get_value(
				"Payment Gateway Account", self.payment_gateway_account, "payment_account"
			)
		elif self.account:  # @dokos
			return self.account

	def before_submit(self):
		if (
			self.currency != self.party_account_currency
			and self.party_account_currency == get_company_currency(self.company)
		):
			# set outstanding amount in party account currency
			invoice = frappe.get_value(
				self.reference_doctype,
				self.reference_name,
				["rounded_total", "grand_total", "base_rounded_total", "base_grand_total"],
				as_dict=1,
			)
			grand_total = invoice.get("rounded_total") or invoice.get("grand_total")
			base_grand_total = invoice.get("base_rounded_total") or invoice.get("base_grand_total")
			self.outstanding_amount = flt(
				self.grand_total / grand_total * base_grand_total,
				self.precision("outstanding_amount"),
			)

		else:
			self.outstanding_amount = self.grand_total

		if self.payment_request_type == "Outward":
			self.status = "Initiated"
		elif self.payment_request_type == "Inward":
			self.status = "Requested"

		if self.payment_request_type == "Inward":
			if self.get("payment_channel") == "Phone":  # @dokos
				self.request_phone_payment()

			else:
				self.set_payment_request_url()
				if not self.message:  # @dokos
					self.mute_email = True

				if not (self.mute_email or self.flags.mute_email):
					self.send_email()
					self.make_communication_entry()

	def on_submit(self):
		self.update_reference_advance_payment_status()

	def request_phone_payment(self):
		controller = _get_payment_gateway_controller(self.payment_gateway)
		request_amount = self.get_request_amount()

		payment_record = dict(
			reference_doctype="Payment Request",
			reference_docname=self.name,
			payment_reference=self.reference_name,
			request_amount=request_amount,
			sender=self.email_to,
			currency=self.currency,
			payment_gateway=self.payment_gateway,
			phone_number=self.phone_number,
		)

		controller.validate_transaction_currency(self.currency)
		controller.request_for_payment(**payment_record)

	def get_request_amount(self):
		data_of_completed_requests = frappe.get_all(
			"Integration Request",
			filters={
				"reference_doctype": self.doctype,
				"reference_docname": self.name,
				"status": "Completed",
			},
			pluck="data",
		)

		if not data_of_completed_requests:
			return self.grand_total

		request_amounts = sum(json.loads(d).get("request_amount") for d in data_of_completed_requests)
		return request_amounts

	def on_cancel(self):
		self.check_if_payment_entry_exists()
		self.set_as_cancelled()
		self.update_reference_advance_payment_status()

		advance_payment_doctypes = frappe.get_hooks("advance_payment_receivable_doctypes") + frappe.get_hooks(
			"advance_payment_payable_doctypes"
		)
		if self.reference_doctype in advance_payment_doctypes:
			# set advance payment status
			ref_doc = frappe.get_doc(self.reference_doctype, self.reference_name)
			ref_doc.set_total_advance_paid()

	def make_invoice(self):
		from erpnext.selling.doctype.sales_order.sales_order import make_sales_invoice

		si = make_sales_invoice(self.reference_name, ignore_permissions=True)
		si.allocate_advances_automatically = True
		si = si.insert(ignore_permissions=True)
		si.submit()

	@frappe.whitelist()
	def check_if_immediate_payment_is_autorized(self):
		if not self.get("payment_gateway"):
			return False

		return self.check_immediate_payment_for_gateway()

	def check_immediate_payment_for_gateway(self) -> bool:
		"""Returns a boolean"""
		try:
			controller = _get_payment_gateway_controller(self.payment_gateway)
			return can_make_immediate_payment(self, controller)
		except frappe.exceptions.ValidationError:
			return False

	@frappe.whitelist()
	def process_payment_immediately(self):
		if (
			frappe.conf.mute_payment_gateways
			or not self.get("payment_gateway")
			or not self.check_immediate_payment_for_gateway()
		):
			return

		return self.get_immediate_payment_for_gateway(self.payment_gateway)

	def get_immediate_payment_for_gateway(self, gateway):
		controller = _get_payment_gateway_controller(gateway)
		if hasattr(controller, "immediate_payment_processing"):
			result = controller.immediate_payment_processing(
				reference=self.name,
				customer=self.get_customer(),
				amount=flt(self.grand_total, self.precision("grand_total")),
				currency=self.currency,
				description=self.subject,
				metadata={
					"reference_doctype": self.doctype,
					"reference_name": self.name,
				},
			)
			if result:
				self.db_set("transaction_reference", result, commit=True)
				self.db_set("status", "Pending", commit=True)
				return result
			else:
				frappe.throw(_("Payment cannot be processed immediately for this payment request."))

	def generate_payment_key(self):
		self.payment_key = frappe.generate_hash(length=70)

	def set_payment_url(self):
		# Keep for compatibility
		self.set_payment_request_url()

	@property
	def customer(self):
		if self.party_type == "Customer":
			return self.party

	@property
	def supplier(self):
		if self.party_type == "Supplier":
			return self.party

	def set_customer(self):
		if customer := self.get_customer():
			self.party_type = "Customer"
			self.party = customer
			self.set_mode_of_payment("Customer", customer)

	def set_supplier(self):
		if supplier := self.get_supplier():
			self.party_type = "Supplier"
			self.party = supplier
			self.set_mode_of_payment("Supplier", supplier)

	def set_mode_of_payment(self, party_type, party):
		if not self.mode_of_payment and (mop := frappe.db.get_value(party_type, party, "mode_of_payment")):
			self.mode_of_payment = mop

	def get_customer(self):
		if frappe.db.has_column(self.reference_doctype, "customer"):
			return frappe.db.get_value(self.reference_doctype, self.reference_name, "customer")

	def get_supplier(self):
		if frappe.db.has_column(self.reference_doctype, "supplier"):
			return frappe.db.get_value(self.reference_doctype, self.reference_name, "supplier")

	def register_default_payment_method(self):
		for dt in ["Customer", "Supplier"]:
			if self.party_type == dt and self.party:
				frappe.db.set_value(self.party_type, self.party, "mode_of_payment", self.mode_of_payment)

		if self.subscription:
			frappe.db.set_value("Subscription", self.subscription, "mode_of_payment", self.mode_of_payment)

	@property
	def reference_document(self):
		return frappe.get_doc(self.reference_doctype, self.reference_name)

	def payment_gateway_validation(self):
		try:
			controller = _get_payment_gateway_controller(self.payment_gateway)
			if hasattr(controller, "on_payment_request_submission"):
				return controller.on_payment_request_submission(self)
			else:
				return True
		except Exception:
			return False

	def set_payment_request_url(self):
		if self.payment_url:
			return

		self.payment_gateway and self.payment_gateway_validation()  # @dokos

		self.payment_url = self.get_payment_request_base_url()  # @dokos

		if self.payment_url:
			self.db_set("payment_url", self.payment_url)

	def get_payment_request_base_url(self):
		return get_url(f"/payments?link={self.payment_key}")

	def get_payment_url(self, payment_gateway):
		self.set_gateway_account()

		data = frappe._dict(
			{
				"title": self.get("company")
				or self.reference_document.get("company")
				or frappe.defaults.get_defaults().company,
				"customer": self.party,
				"customer_name": frappe.db.get_value("Customer", self.party, "customer_name"),
			}
		)

		controller = _get_payment_gateway_controller(payment_gateway)
		controller.validate_transaction_currency(self.currency)

		if hasattr(controller, "validate_minimum_transaction_amount"):
			controller.validate_minimum_transaction_amount(self.currency, self.grand_total)

		# All keys kept for backward compatibility
		return controller.get_payment_url(
			**{
				"amount": flt(self.grand_total, self.precision("grand_total")),
				"title": data.title,
				"description": self.subject,
				"reference_doctype": "Payment Request",
				"reference_docname": self.name,
				"payer_email": self.email_to or frappe.session.user,
				"payer_name": data.customer_name,
				"order_id": self.name,
				"currency": self.currency,
				"payment_key": self.payment_key,
				"payment_gateway": self.payment_gateway,
			}
		)

	@frappe.whitelist()
	def set_as_paid(self, reference_no=None):
		payment_entry = None
		if reference_no and (
			existing_pe := frappe.db.get_value(
				"Payment Entry", filters={"docstatus": 1, "reference_no": reference_no}
			)
		):
			payment_entry = frappe.get_doc("Payment Entry", existing_pe)

		if not payment_entry:
			frappe.flags.mute_messages = True
			if reference_no:
				self.register_customer(reference_no)

			payment_entry = self.create_payment_entry(reference_no=reference_no)
			if self.make_sales_invoice:
				self.make_invoice()
			frappe.flags.mute_messages = False

		if self.status != "Paid":
			self.db_set("status", "Paid", commit=True)

		return payment_entry

	def register_customer(self, reference_no):
		if frappe.flags.in_test:
			return

		controller = _get_payment_gateway_controller(self.payment_gateway)
		if customer_id := controller.run_method("get_customer_id", reference_no):
			customer = frappe.get_cached_doc("Customer", self.customer)
			if not customer.get(
				"payment_gateways_references",
				dict(payment_gateway=self.payment_gateway, customer_id=customer_id),
			):
				customer.append(
					"payment_gateways_references",
					{"payment_gateway": self.payment_gateway, "customer_id": customer_id},
				)
			if not customer.mode_of_payment:
				customer.mode_of_payment = get_mode_of_payment_for_company(self.company, self.payment_gateway)
			customer.flags.ignore_permissions = True
			customer.save()

	def create_payment_entry(self, submit=True, reference_no=None):
		"""create entry"""
		frappe.flags.ignore_account_permission = True
		frappe.flags.ignore_permissions = True

		ref_doc = frappe.get_doc(self.reference_doctype, self.reference_name)

		if self.reference_doctype in ["Sales Invoice", "POS Invoice"]:
			party_account = ref_doc.debit_to
		elif self.reference_doctype == "Purchase Invoice":
			party_account = ref_doc.credit_to
		else:
			party_account = get_party_account(
				"Customer", ref_doc.get("customer"), ref_doc.company, tax_category=ref_doc.get("tax_category")
			)  # @dokos

		party_account_currency = (
			self.get("party_account_currency")
			or ref_doc.get("party_account_currency")
			or get_account_currency(party_account)
		)

		party_amount = bank_amount = self.outstanding_amount

		# @dokos
		mode_of_payment_defaults = (
			frappe.db.get_value(
				"Mode of Payment Account",
				dict(parent=self.mode_of_payment, company=ref_doc.company),
				["default_account", "fee_account", "tax_account", "cost_center"],
				as_dict=1,
			)
			if self.mode_of_payment
			else dict()
		)

		self.get_payment_gateway_fees(reference_no)
		total_fees = self.get_total_fees(mode_of_payment_defaults)
		# @dokos

		if party_account_currency == ref_doc.company_currency and party_account_currency != self.currency:
			exchange_rate = ref_doc.get("conversion_rate")
			bank_amount = (
				flt(self.outstanding_amount / exchange_rate, self.precision("grand_total")) - total_fees
			)  # @dokos

		# outstanding amount is already in Part's account currency
		payment_entry = get_payment_entry(
			self.reference_doctype,
			self.reference_name,
			party_amount=party_amount,
			bank_account=mode_of_payment_defaults.get("default_account") or self.get_payment_account(),
			bank_amount=bank_amount,
			created_from_payment_request=True,
		)

		payment_entry.setup_party_account_field()
		payment_entry.set_missing_values()
		payment_entry.set_exchange_rate()

		payment_entry.update(
			{
				"mode_of_payment": self.mode_of_payment,
				"reference_no": reference_no
				if reference_no
				else (self.transaction_reference or self.name),  # @dokos
				"reference_date": nowdate(),
				"remarks": _("Payment Entry against {0} {1} via Payment Request {2}").format(
					self.reference_doctype, self.reference_name, self.name
				),
			}
		)

		# Allocate payment_request for each reference in payment_entry (Payment Term can splits the row)
		self._allocate_payment_request_to_pe_references(references=payment_entry.references)

		# @dokos
		if total_fees:
			for value in ["fee", "tax"]:
				if mode_of_payment_defaults.get(f"{value}_account") and self.get(f"{value}_amount"):
					payment_entry.append(
						"deductions",
						{
							"account": mode_of_payment_defaults.get(f"{value}_account"),
							"cost_center": mode_of_payment_defaults.get("cost_center"),
							"amount": self.get(f"{value}_amount"),
						},
					)
		# @dokos

		# Update dimensions
		payment_entry.update(
			{
				"cost_center": self.get("cost_center"),
				"project": self.get("project"),
			}
		)

		# @dokos
		if total_fees:
			payment_entry.update(
				{
					"paid_amount": flt(payment_entry.paid_amount) - total_fees,
					"received_amount": flt(payment_entry.received_amount) - total_fees,
				}
			)
		# @dokos

		payment_entry.set_amounts()

		# Update 'Paid Amount' on Forex transactions
		if self.currency != ref_doc.company_currency:
			if (
				self.payment_request_type == "Outward"
				and payment_entry.paid_from_account_currency == ref_doc.company_currency
				and payment_entry.paid_from_account_currency != payment_entry.paid_to_account_currency
			):
				payment_entry.paid_amount = payment_entry.base_paid_amount = (
					payment_entry.target_exchange_rate * payment_entry.received_amount
				) - flt(total_fees)  # @dokos

		payment_entry.set_total_allocated_amount()
		payment_entry.set_unallocated_amount()
		payment_entry.set_difference_amount()

		payment_entry.payment_request = self.name  # @dokos

		for dimension in get_accounting_dimensions():
			payment_entry.update({dimension: self.get(dimension)})
		if submit:
			payment_entry.insert(ignore_permissions=True)
			payment_entry.submit()

		return payment_entry

	def get_total_fees(self, mode_of_payment_defaults):
		total_fee_amount = 0.0
		for value in ["fee", "tax"]:
			if (
				self.get(f"{value}_amount")
				and mode_of_payment_defaults.get(f"{value}_account")
				and mode_of_payment_defaults.get("cost_center")
			):
				total_fee_amount += flt(self.get(f"{value}_amount")) * flt(
					self.get("target_exchange_rate", 1)
				)

		return total_fee_amount

	def get_payment_gateway_fees(self, reference_no=None):
		if self.payment_gateway and reference_no:
			controller = _get_payment_gateway_controller(self.payment_gateway)
			if hasattr(controller, "get_transaction_fees"):
				transaction_fees = controller.get_transaction_fees(reference_no)
				if not self.fee_amount:
					self.fee_amount = 0.0

				self.fee_amount += transaction_fees.fee_amount
				self.base_amount = transaction_fees.base_amount or self.base_amount
				self.tax_amount = transaction_fees.get("tax_amount")
				if transaction_fees.target_exchange_rate:
					self.target_exchange_rate = transaction_fees.target_exchange_rate

	def send_email(self, communication=None):
		"""send email with payment link"""
		email_args = {
			"recipients": self.email_to,
			"sender": frappe.session.user if frappe.session.user not in ("Administrator", "Guest") else None,
			"subject": self.subject,
			"message": self.message,
			"now": True,
			"communication": communication.name if communication else None,
			"attachments": [
				frappe.attach_print(
					self.reference_doctype,
					self.reference_name,
					file_name=self.reference_name,
					print_format=self.print_format,
				)
			]
			if self.print_format
			else [],  # @dokos
		}
		enqueue(
			method=frappe.sendmail,
			queue="short",
			timeout=300,
			is_async=True,
			enqueue_after_commit=True,
			**email_args,
		)

	def get_message(self):  # @dokos
		"""return message with payment gateway link"""
		return get_message(self, self.template)

	def set_failed(self):
		self.db_set("status", "Failed")

	def set_as_cancelled(self):
		self.db_set("status", "Cancelled")

	def check_if_payment_entry_exists(self):
		if self.status == "Paid":
			if frappe.get_all(
				"Payment Entry Reference",
				filters={"reference_name": self.reference_name, "docstatus": ["<", 2]},
				fields=["parent"],
				limit=1,
			):
				frappe.throw(_("A payment entry for this reference exists already"), title=_("Error"))

	def make_communication_entry(self):
		"""Make communication entry"""
		try:
			comm = frappe.get_doc(
				{
					"doctype": "Communication",
					"communication_medium": "Email",
					"recipients": self.email_to,
					"subject": self.subject,
					"content": self.message,
					"sent_or_received": "Sent",
					"reference_doctype": self.reference_doctype,
					"reference_name": self.reference_name,
				}
			)
			comm.insert(ignore_permissions=True)
			return comm
		except Exception:
			comm.log_error(_("Payment request communication creation error"))

	def get_payment_success_url(self):
		return self.payment_success_url

	def on_payment_authorized(self, status=None, reference_no=None):
		if not status:
			return

		PAID_STATUSES = ("Authorized", "Completed", "Paid")
		curr_status, next_status = self.status, status
		is_not_draft = not self.docstatus.is_draft()
		if next_status in PAID_STATUSES:
			self.run_method("set_as_paid", reference_no)
		elif (curr_status == "Requested") and (next_status == "Pending") and is_not_draft:
			self.db_set("status", "Pending", commit=True)
		elif (curr_status in ("Pending", "Requested")) and (next_status == "Payment Method Registered"):
			if curr_status == "Requested":
				self.db_set("status", "Pending", commit=True)
			self.run_method("set_payment_method_registered")
		elif next_status == "Failed":
			self.db_set("status", "Failed", commit=True)

		if not reference_no:
			self.db_set("transaction_reference", reference_no, commit=True)

		if curr_status in ("Pending", "Paid"):
			self.register_default_payment_method()

		return self.get_redirection()

	def set_payment_method_registered(self):
		"""Called when payment method is registered for off-session payments"""
		self.process_payment_immediately()

	def get_redirection(self):
		redirect_to = "no-redirection"

		try:
			# if shopping cart enabled and in session
			shopping_cart_settings = _get_shopping_cart_settings()

			if (
				shopping_cart_settings.get("enabled")
				and hasattr(frappe.local, "session")
				and frappe.local.session.user != "Guest"
			):
				success_url = shopping_cart_settings.get("payment_success_url")
				if success_url:
					redirect_to = ({"Orders": "/orders", "Invoices": "/invoices", "My Account": "/me"}).get(
						success_url, "/me"
					)
				else:
					redirect_to = get_url(f"/orders/{self.reference_name}")
		except Exception:
			frappe.clear_messages()
			redirect_to = "payment-success"
			controller = _get_payment_gateway_controller(self.payment_gateway)
			if controller.doctype == "Stripe Settings" and controller.redirect_url:
				redirect_to = controller.redirect_url

		return redirect_to

	def set_message_from_template(self):
		data = frappe._dict(get_message(self, self.email_template))
		self.subject, self.message = data.subject, data.message

	def set_payment_gateway_from_mode_of_payment(self):
		if self.mode_of_payment:
			if payment_gateway := frappe.get_cached_value(
				"Mode of Payment Account",
				{"company": self.company, "parent": self.mode_of_payment},
				"payment_gateway",
			):
				self.payment_gateway = payment_gateway

	def update_reference_advance_payment_status(self):
		advance_payment_doctypes = frappe.get_hooks("advance_payment_receivable_doctypes") + frappe.get_hooks(
			"advance_payment_payable_doctypes"
		)
		if self.reference_doctype in advance_payment_doctypes:
			ref_doc = frappe.get_doc(self.reference_doctype, self.reference_name)
			ref_doc.set_advance_payment_status()

	def _allocate_payment_request_to_pe_references(self, references):
		"""
		Allocate the Payment Request to the Payment Entry references based on\n
		    - Allocated Amount.
		    - Outstanding Amount of Payment Request.\n
		Payment Request is doc itself and references are the rows of Payment Entry.
		"""
		if len(references) == 1:
			references[0].payment_request = self.name
			return

		precision = references[0].precision("allocated_amount")
		outstanding_amount = self.outstanding_amount

		# to manage rows
		row_number = 1
		MOVE_TO_NEXT_ROW = 1
		TO_SKIP_NEW_ROW = 2
		NEW_ROW_ADDED = False

		while row_number <= len(references):
			row = references[row_number - 1]

			# update the idx to maintain the order
			row.idx = row_number

			if outstanding_amount == 0:
				if not NEW_ROW_ADDED:
					break

				row_number += MOVE_TO_NEXT_ROW
				continue

			# allocate the payment request to the row
			row.payment_request = self.name

			if row.allocated_amount <= outstanding_amount:
				outstanding_amount = flt(outstanding_amount - row.allocated_amount, precision)
				row_number += MOVE_TO_NEXT_ROW
			else:
				remaining_allocated_amount = flt(row.allocated_amount - outstanding_amount, precision)
				row.allocated_amount = outstanding_amount
				outstanding_amount = 0

				# create a new row without PR for remaining unallocated amount
				new_row = frappe.copy_doc(row)
				references.insert(row_number, new_row)

				# update new row
				new_row.idx = row_number + 1
				new_row.payment_request = None
				new_row.allocated_amount = remaining_allocated_amount

				NEW_ROW_ADDED = True
				row_number += TO_SKIP_NEW_ROW

	def get_subscription_reference(self):
		if self.subscription:
			return

		meta = frappe.get_meta(self.reference_doctype)
		if meta.has_field("subscription"):
			self.subscription = frappe.db.get_value(
				self.reference_doctype, self.reference_name, "subscription"
			)


@frappe.whitelist(allow_guest=True)
def get_or_make_payment_request(*args, **kwargs):
	if kwargs.get("order_type") == "Shopping Cart":
		if existing_payment_request := frappe.db.get_value(
			"Payment Request",
			{
				"reference_doctype": kwargs.get("dt"),
				"reference_name": kwargs.get("dn"),
				"docstatus": ("!=", 2),
			},
		):
			pr = frappe.get_doc("Payment Request", existing_payment_request)
			ref_doc = frappe.get_doc(kwargs.get("dt"), kwargs.get("dn"))
			grand_total = flt(kwargs.get("grand_total")) or get_amount(ref_doc)
			frappe.db.set_value(
				"Payment Request", existing_payment_request, "grand_total", grand_total, update_modified=False
			)

			pr.set_payment_url()
			frappe.db.commit()
			frappe.local.response["type"] = "redirect"
			frappe.local.response["location"] = pr.payment_url
			return

	return make_payment_request(*args, **kwargs)


@frappe.whitelist(allow_guest=True)
def make_payment_request(**args):
	"""Make payment request"""

	args = frappe._dict(args)
	if args.dt not in ALLOWED_DOCTYPES_FOR_PAYMENT_REQUEST:
		frappe.throw(_("Payment Requests cannot be created against: {0}").format(frappe.bold(_(args.dt))))

	ref_doc = args.ref_doc or frappe.get_doc(args.dt, args.dn)

	gateway_account = {}
	if args.get("payment_gateway") or args.get("payment_gateway_account") or args.get("mode_of_payment"):
		gateway_account = get_gateway_details(args) or frappe._dict()

	grand_total = get_amount(ref_doc, gateway_account.get("payment_account"))
	if not grand_total:
		frappe.throw(_("Payment Entry is already created"))

	if args.loyalty_points and ref_doc.doctype == "Sales Order":
		from erpnext.accounts.doctype.loyalty_program.loyalty_program import validate_loyalty_points

		loyalty_amount = validate_loyalty_points(ref_doc, int(args.loyalty_points))  # sets fields on ref_doc
		ref_doc.db_update()
		grand_total = grand_total - loyalty_amount

	bank_account = (
		get_party_bank_account(args.get("party_type"), args.get("party")) if args.get("party_type") else ""
	)

	draft_payment_request = frappe.db.get_value(
		"Payment Request",
		{"reference_doctype": ref_doc.doctype, "reference_name": ref_doc.name, "docstatus": 0},
	)

	# fetches existing payment request `grand_total` amount
	existing_payment_request_amount = get_existing_payment_request_amount(ref_doc.doctype, ref_doc.name)

	existing_paid_amount = get_existing_paid_amount(ref_doc.doctype, ref_doc.name)

	def validate_and_calculate_grand_total(grand_total, existing_payment_request_amount):
		grand_total -= existing_payment_request_amount
		if not grand_total:
			frappe.throw(_("Payment Request is already created"))
		return grand_total

	if existing_payment_request_amount:
		if args.order_type == "Shopping Cart":
			# If Payment Request is in an advanced stage, then create for remaining amount.
			if get_existing_payment_request_amount(
				ref_doc.doctype, ref_doc.name, ["Initiated", "Partially Paid", "Payment Ordered", "Paid"]
			):
				grand_total = validate_and_calculate_grand_total(grand_total, existing_payment_request_amount)
			else:
				# If PR's are processed, cancel all of them.
				cancel_old_payment_requests(ref_doc.doctype, ref_doc.name)
		else:
			grand_total = validate_and_calculate_grand_total(grand_total, existing_payment_request_amount)

	if existing_paid_amount:
		if ref_doc.party_account_currency == ref_doc.currency:
			if ref_doc.conversion_rate:
				grand_total -= flt(existing_paid_amount / ref_doc.conversion_rate)
			else:
				grand_total -= flt(existing_paid_amount)
		else:
			grand_total -= flt(existing_paid_amount / ref_doc.conversion_rate)

	if draft_payment_request:
		frappe.db.set_value(
			"Payment Request", draft_payment_request, "grand_total", grand_total, update_modified=False
		)
		pr = frappe.get_doc("Payment Request", draft_payment_request)
	else:
		email_to = args.recipient_id or ref_doc.get("contact_email") or ref_doc.owner
		if email_to in ["Administrator", "Guest"]:
			email_to = None

		pr = frappe.new_doc("Payment Request")

		if not args.get("payment_request_type"):
			args["payment_request_type"] = (
				"Outward" if args.get("dt") in ["Purchase Order", "Purchase Invoice"] else "Inward"
			)

		make_sales_invoice = True
		if ref_doc.doctype == "Sales Invoice":  # @dokos
			make_sales_invoice = False  # @dokos
		elif hasattr(args, "make_sales_invoice"):
			make_sales_invoice = args.get("make_sales_invoice")

		party_type = args.get("party_type") or "Customer"
		party_account_currency = ref_doc.get("party_account_currency")

		if not party_account_currency:
			party_account = get_party_account(party_type, ref_doc.get(party_type.lower()), ref_doc.company)
			party_account_currency = get_account_currency(party_account)

		pr.update(
			{
				"payment_gateway_account": gateway_account.get("name"),
				"payment_gateway": gateway_account.get("payment_gateway"),
				"payment_account": gateway_account.get("payment_account"),
				"payment_channel": gateway_account.get("payment_channel"),
				"payment_request_type": "Outward"
				if ref_doc.doctype
				in [*frappe.get_hooks("advance_payment_payable_doctypes"), "Purchase Invoice"]  # @dokos
				else "Inward",
				"currency": args.currency or ref_doc.currency,
				"party_account_currency": party_account_currency,
				"grand_total": grand_total,
				"email_to": email_to,
				"subject": _("Payment Request for {0}").format(args.dn),
				"reference_doctype": ref_doc.doctype,
				"reference_name": ref_doc.name,
				"company": ref_doc.get("company"),
				"email_template": args.email_template,
				"message": args.message,
				"print_format": args.print_format,
				"subscription": args.subscription,
				"mode_of_payment": args.get("mode_of_payment"),
				"party_type": party_type,
				"party": args.get("party") or ref_doc.get("customer"),
				"bank_account": bank_account,
				"party_name": args.get("party_name") or ref_doc.get("customer_name"),
				"make_sales_invoice": make_sales_invoice,  # new standard @dokos
				"mute_email": (
					args.mute_email  # new standard
					or args.order_type == "Shopping Cart"  # compat for webshop app
					or gateway_account.get("payment_channel", "Email") != "Email"
				),
				"phone_number": args.get("phone_number"),
			}
		)

		# Update dimensions
		pr.update(
			{
				"cost_center": ref_doc.get("cost_center"),
				"project": ref_doc.get("project"),
			}
		)

		for dimension in get_accounting_dimensions():
			pr.update({dimension: ref_doc.get(dimension)})

		if args.order_type == "Shopping Cart" or args.mute_email:
			pr.flags.mute_email = True  # @dokos

		# @dokos: No need for create_pr_in_draft_status

		if args.submit_doc:
			pr.insert(ignore_permissions=True)
			pr.submit()

	if args.order_type == "Shopping Cart" and not args.return_doc:
		pr.set_payment_url()
		frappe.db.commit()
		frappe.local.response["type"] = "redirect"
		frappe.local.response["location"] = pr.payment_url

	if args.return_doc:
		return pr

	return pr.as_dict()


@frappe.whitelist()
def get_reference_amount(doctype, docname):
	ref_doc = frappe.get_doc(doctype, docname)
	return get_amount(ref_doc)


def get_amount(ref_doc, payment_account=None):
	"""get amount based on doctype"""
	dt = ref_doc.doctype
	if dt in ["Sales Order", "Purchase Order"]:
		grand_total = flt(ref_doc.rounded_total or ref_doc.grand_total) - flt(ref_doc.advance_paid)  # @dokos
	elif dt in ["Sales Invoice", "Purchase Invoice"]:
		if not ref_doc.get("is_pos"):
			if ref_doc.party_account_currency == ref_doc.currency:
				grand_total = flt(ref_doc.rounded_total or ref_doc.grand_total)
			else:
				grand_total = flt(
					flt(ref_doc.base_rounded_total or ref_doc.base_grand_total) / ref_doc.conversion_rate
				)
		elif dt == "Sales Invoice":
			for pay in ref_doc.payments:
				if pay.type == "Phone" and pay.account == payment_account:
					grand_total = pay.amount
					break
	elif dt == "POS Invoice":
		for pay in ref_doc.payments:
			if pay.type == "Phone" and pay.account == payment_account:
				grand_total = pay.amount
				break
	elif dt == "Fees":
		grand_total = ref_doc.outstanding_amount

	if grand_total > 0:
		return flt(grand_total, get_currency_precision())
	else:
		frappe.throw(_("There is no outstanding amount for this reference"))


def get_irequest_status(payment_requests: None | list = None) -> list:
	IR = frappe.qb.DocType("Integration Request")
	res = []
	if payment_requests:
		res = (
			frappe.qb.from_(IR)
			.select(IR.name)
			.where(IR.reference_doctype.eq("Payment Request"))
			.where(IR.reference_docname.isin(payment_requests))
			.where(IR.status.isin(["Authorized", "Completed"]))
			.run(as_dict=True)
		)
	return res


def cancel_old_payment_requests(ref_dt, ref_dn):
	PR = frappe.qb.DocType("Payment Request")

	if res := (
		frappe.qb.from_(PR)
		.select(PR.name)
		.where(PR.reference_doctype == ref_dt)
		.where(PR.reference_name == ref_dn)
		.where(PR.docstatus == 1)
		.where(PR.status.isin(["Draft", "Requested"]))
		.run(as_dict=True)
	):
		if get_irequest_status([x.name for x in res]):
			frappe.throw(_("Another Payment Request is already processed"))
		else:
			for x in res:
				doc = frappe.get_doc("Payment Request", x.name)
				doc.flags.ignore_permissions = True
				doc.cancel()

				if ireqs := get_irequests_of_payment_request(doc.name):
					for ireq in ireqs:
						frappe.db.set_value("Integration Request", ireq.name, "status", "Cancelled")


def get_existing_payment_request_amount(ref_dt, ref_dn, statuses: list | None = None) -> list:
	"""
	Return the total amount of Payment Requests against a reference document.
	"""
	PR = frappe.qb.DocType("Payment Request")

	query = (
		frappe.qb.from_(PR)
		.select(Sum(PR.grand_total))
		.where(PR.reference_doctype == ref_dt)
		.where(PR.reference_name == ref_dn)
		.where(PR.docstatus == 1)
	)

	if statuses:
		query = query.where(PR.status.isin(statuses))

	response = query.run()

	return response[0][0] if response[0] else 0


def get_existing_paid_amount(doctype, name):
	PLE = frappe.qb.DocType("Payment Ledger Entry")
	PER = frappe.qb.DocType("Payment Entry Reference")

	query = (
		frappe.qb.from_(PLE)
		.left_join(PER)
		.on(
			(PLE.against_voucher_type == PER.reference_doctype)
			& (PLE.against_voucher_no == PER.reference_name)
			& (PLE.voucher_type == PER.parenttype)
			& (PLE.voucher_no == PER.parent)
		)
		.select(
			Abs(Sum(PLE.amount)).as_("total_amount"),
			Abs(Sum(frappe.qb.terms.Case().when(PER.payment_request.isnotnull(), PLE.amount).else_(0))).as_(
				"request_paid_amount"
			),
		)
		.where(
			(PLE.voucher_type.isin([doctype, "Journal Entry", "Payment Entry"]))
			& (PLE.against_voucher_type == doctype)
			& (PLE.against_voucher_no == name)
			& (PLE.delinked == 0)
			& (PLE.docstatus == 1)
			& (PLE.amount < 0)
		)
	)

	result = query.run()
	ledger_amount = flt(result[0][0]) if result else 0
	request_paid_amount = flt(result[0][1]) if result else 0

	return ledger_amount - request_paid_amount


def get_gateway_details(args):  # nosemgrep
	"""return gateway and payment account of default payment gateway"""
	filters = {}
	if args.get("currency"):
		filters.update({"currency": args.get("currency")})

	if gateway_account := args.get("payment_gateway_account"):
		return get_payment_gateway_account(gateway_account)

	if args.get("payment_gateway"):
		filters.update({"payment_gateway": args.get("payment_gateway")})
		return get_payment_gateway_account(filters)

	if args.get("mode_of_payment") and (
		payment_gateway := frappe.db.get_value(
			"Mode of Payment", args.get("mode_of_payment"), "payment_gateway"
		)
	):
		filters.update({"payment_gateway": payment_gateway})
		return get_payment_gateway_account(filters)

	filters.update({"is_default": 1})
	gateway_account = get_payment_gateway_account(filters)

	return gateway_account


def get_payment_gateway_account(args):
	return frappe.db.get_value(
		"Payment Gateway Account", args, ["name", "payment_gateway", "payment_channel"], as_dict=1
	)


@frappe.whitelist()
def get_print_format_list(ref_doctype):
	print_format_list = ["Standard"]

	print_format_list.extend(
		[p.name for p in frappe.get_all("Print Format", filters={"doc_type": ref_doctype})]
	)

	return {"print_format": print_format_list}


@frappe.whitelist(allow_guest=True)
def resend_payment_email(docname):
	doc = frappe.get_doc("Payment Request", docname)
	communication = doc.make_communication_entry()
	return doc.send_email(communication)


@frappe.whitelist()
def make_payment_entry(docname):
	doc = frappe.get_doc("Payment Request", docname)
	return doc.create_payment_entry(submit=False, reference_no=doc.transaction_reference).as_dict()


def make_status_as_paid(doc, method):
	warnings.warn(
		"make_status_as_paid will be deprecated since it is error prone.", FutureWarning, stacklevel=2
	)
	for ref in doc.references:
		payment_request_name = frappe.db.get_value(
			"Payment Request",
			{
				"reference_doctype": ref.reference_doctype,
				"reference_name": ref.reference_name,
				"docstatus": 1,
			},
		)

		if payment_request_name:
			doc = frappe.get_doc("Payment Request", payment_request_name)
			if doc.status != "Paid":
				doc.db_set("status", "Paid")
				frappe.db.commit()


def update_payment_requests_as_per_pe_references(references=None, cancel=False):
	"""
	Update Payment Request's `Status` and `Outstanding Amount` based on Payment Entry Reference's `Allocated Amount`.
	"""
	if not references:
		return

	precision = references[0].precision("allocated_amount")

	referenced_payment_requests = frappe.get_all(
		"Payment Request",
		filters={"name": ["in", {row.payment_request for row in references if row.payment_request}]},
		fields=[
			"name",
			"grand_total",
			"outstanding_amount",
			"payment_request_type",
		],
	)

	referenced_payment_requests = {pr.name: pr for pr in referenced_payment_requests}

	for ref in references:
		if not ref.payment_request:
			continue

		payment_request = referenced_payment_requests[ref.payment_request]
		pr_outstanding = payment_request["outstanding_amount"]

		# update outstanding amount
		new_outstanding_amount = flt(
			pr_outstanding + ref.allocated_amount if cancel else pr_outstanding - ref.allocated_amount,
			precision,
		)

		# to handle same payment request for the multiple allocations
		payment_request["outstanding_amount"] = new_outstanding_amount

		if not cancel and new_outstanding_amount < 0:
			frappe.throw(
				msg=_(
					"The allocated amount is greater than the outstanding amount of Payment Request {0}"
				).format(ref.payment_request),
				title=_("Invalid Allocated Amount"),
			)

		# update status
		if new_outstanding_amount == payment_request["grand_total"]:
			status = "Initiated" if payment_request["payment_request_type"] == "Outward" else "Requested"
		elif new_outstanding_amount == 0:
			status = "Paid"
		elif new_outstanding_amount > 0:
			status = "Partially Paid"

		# update database
		frappe.db.set_value(
			"Payment Request",
			ref.payment_request,
			{"outstanding_amount": new_outstanding_amount, "status": status},
		)


@frappe.whitelist()
def make_status_as_completed(name):
	frappe.db.set_value("Payment Request", name, "status", "Completed")


@frappe.whitelist()
def get_message(doc, template):
	"""return message with payment gateway link"""
	payment_can_be_processed_immediately = None

	if isinstance(doc, str):
		doc = json.loads(doc)
	elif isinstance(doc, Document):
		payment_can_be_processed_immediately = doc.check_if_immediate_payment_is_autorized()
		doc = doc.as_dict()

	context = dict(
		doc,
		**{
			"doc": doc,
			"reference": frappe.get_doc(doc.get("reference_doctype"), doc.get("reference_name")),
			"payment_link": doc.get("payment_url"),
			"payment_can_be_processed_immediately": bool(payment_can_be_processed_immediately),
		},
	)

	email_template = frappe.get_doc("Email Template", template)

	return {
		"subject": frappe.render_template(email_template.subject, context),
		"message": frappe.render_template(email_template.response, context),
	}


@frappe.whitelist()
def check_if_immediate_payment_is_autorized(payment_request):
	return frappe.get_doc("Payment Request", payment_request).check_if_immediate_payment_is_autorized()


@frappe.whitelist()
def make_payment_order(source_name, target_doc=None):
	from frappe.model.mapper import get_mapped_doc

	def set_missing_values(source, target):
		target.payment_order_type = "Payment Request"
		bank_account = source.bank_account or frappe.db.get_value(
			source.party_type, source.party, "default_bank_account"
		)
		target.append(
			"references",
			{
				"reference_doctype": source.reference_doctype,
				"reference_name": source.reference_name,
				"amount": source.grand_total,
				"supplier": source.party,
				"payment_request": source_name,
				"mode_of_payment": source.mode_of_payment,
				"bank_account": bank_account,
				"account": source.account,
			},
		)

	doclist = get_mapped_doc(
		"Payment Request",
		source_name,
		{
			"Payment Request": {
				"doctype": "Payment Order",
			}
		},
		target_doc,
		set_missing_values,
	)

	return doclist


def validate_payment(doc, method=None):
	if doc.reference_doctype != "Payment Request" or (
		frappe.db.get_value(doc.reference_doctype, doc.reference_docname, "status") != "Paid"
	):
		return

	frappe.throw(
		_("The Payment Request {0} is already paid, cannot process payment twice").format(
			doc.reference_docname
		)
	)


@frappe.whitelist()
def get_open_payment_requests_query(doctype, txt, searchfield, start, page_len, filters):
	# permission checks in `get_list()`
	filters = frappe._dict(filters)

	if not filters.reference_doctype or not filters.reference_name:
		return []

	if txt:
		filters.name = ["like", f"%{txt}%"]

	open_payment_requests = frappe.get_list(
		"Payment Request",
		filters=filters,
		fields=["name", "grand_total", "outstanding_amount"],
		order_by="transaction_date ASC,creation ASC",
	)

	return [
		(
			pr.name,
			_("<strong>Grand Total:</strong> {0}").format(pr.grand_total),
			_("<strong>Outstanding Amount:</strong> {0}").format(pr.outstanding_amount),
		)
		for pr in open_payment_requests
	]


def get_irequests_of_payment_request(doc: str | None = None) -> list:
	res = []
	if doc:
		res = frappe.db.get_all(
			"Integration Request",
			{
				"reference_doctype": "Payment Request",
				"reference_docname": doc,
				"status": "Queued",
			},
		)
	return res


def get_mode_of_payment_for_company(company, payment_gateway):
	mode_of_payment = frappe.qb.DocType("Mode of Payment")
	mode_of_payment_account = frappe.qb.DocType("Mode of Payment Account")
	payment_gateway_dt = frappe.qb.DocType("Payment Gateway")
	query = (
		frappe.qb.from_(mode_of_payment_account)
		.left_join(mode_of_payment)
		.on(mode_of_payment_account.parent == mode_of_payment.name)
		.left_join(payment_gateway_dt)
		.on(mode_of_payment_account.payment_gateway == payment_gateway_dt.name)
		.select(mode_of_payment.name)
		.where(
			mode_of_payment.enabled
			& (mode_of_payment_account.company == company)
			& (mode_of_payment_account.payment_gateway == payment_gateway)
			& (payment_gateway_dt.disabled == 0)
		)
	)

	mode_of_payments = query.run(as_dict=True, pluck=True)

	return mode_of_payments[0] if mode_of_payments else None
