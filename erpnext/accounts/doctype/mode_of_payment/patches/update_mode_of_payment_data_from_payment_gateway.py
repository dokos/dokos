from collections import defaultdict

import frappe


def execute():
	"""Update Mode of Payment Data from Payment Gateway"""
	pg_map = defaultdict(dict)
	for payment_gateway in frappe.get_all(
		"Payment Gateway", filters={"mode_of_payment": ("is", "set")}, fields=["*"]
	):
		pg_map[payment_gateway.name] = payment_gateway.mode_of_payment
		mop_doc = frappe.get_doc("Mode of Payment", payment_gateway.mode_of_payment)
		mop_doc.icon = payment_gateway.get("icon")
		for field in ["fee_account", "tax_account"]:
			if mop_doc.get(field):
				company = frappe.db.get_value("Account", mop_doc.get(field), "company")
				for account_row in mop_doc.accounts:
					if account_row.company == company:
						account_row.set(field, mop_doc.get(field))

		if mop_doc.get("cost_center"):
			company = frappe.db.get_value("Cost Center", mop_doc.get("cost_center"), "company")
			for account_row in mop_doc.accounts:
				if account_row.company == company:
					account_row.set(field, mop_doc.get(field))

		mop_doc.save()

	for subscription in frappe.get_all(
		"Subscription", filters={"payment_gateway": ("is", "set")}, fields=["payment_gateway", "name"]
	):
		frappe.db.set_value(
			"Subscription", subscription.name, "mode_of_payment", pg_map.get(subscription.payment_gateway)
		)
