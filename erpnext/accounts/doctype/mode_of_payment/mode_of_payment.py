# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt


import frappe
from frappe import _
from frappe.model.document import Document


class ModeofPayment(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.accounts.doctype.mode_of_payment_account.mode_of_payment_account import (
			ModeofPaymentAccount,
		)

		accounts: DF.Table[ModeofPaymentAccount]
		enabled: DF.Check
		icon: DF.Literal["Credit Card", "Wire Transfer", "Bank Draft", "Cash", "Cheque", "Phone", "Other"]
		mode_of_payment: DF.Data
		portal_description: DF.MarkdownEditor | None
		portal_title: DF.Data | None
		type: DF.Literal["Cash", "Bank", "Phone"]
	# end: auto-generated types

	def validate(self):
		self.validate_accounts()
		self.validate_repeating_companies()
		self.validate_pos_mode_of_payment()

	def validate_repeating_companies(self):
		"""Error when Same Company is entered multiple times in accounts"""
		accounts_list = []
		for entry in self.accounts:
			accounts_list.append(entry.company)

		if len(accounts_list) != len(set(accounts_list)):
			frappe.throw(_("Same Company is entered more than once"))

	def validate_accounts(self):
		for entry in self.accounts:
			"""Error when Company of Ledger account doesn't match with Company Selected"""
			if frappe.get_cached_value("Account", entry.default_account, "company") != entry.company:
				frappe.throw(
					_("Account {0} does not match with Company {1} in Mode of Account: {2}").format(
						entry.default_account, entry.company, self.name
					)
				)

	def validate_pos_mode_of_payment(self):
		if not self.enabled:
			pos_profiles = frappe.db.sql(
				"""SELECT sip.parent FROM `tabSales Invoice Payment` sip
				WHERE sip.parenttype = 'POS Profile' and sip.mode_of_payment = %s""",
				(self.name),
			)
			pos_profiles = list(map(lambda x: x[0], pos_profiles))

			if pos_profiles:
				message = _(
					"POS Profile {} contains Mode of Payment {}. Please remove them to disable this mode."
				).format(frappe.bold(", ".join(pos_profiles)), frappe.bold(str(self.name)))
				frappe.throw(message, title=_("Not Allowed"))

	def get_company_default_account(self, company):
		for account in self.accounts:
			if account.company == company:
				return account.default_account


def get_authorized_modes_of_payment(company=None):
	mode_of_payment = frappe.qb.DocType("Mode of Payment")
	mode_of_payment_account = frappe.qb.DocType("Mode of Payment Account")
	payment_gateway = frappe.qb.DocType("Payment Gateway")
	query = (
		frappe.qb.from_(mode_of_payment_account)
		.left_join(mode_of_payment)
		.on(mode_of_payment_account.parent == mode_of_payment.name)
		.left_join(payment_gateway)
		.on(mode_of_payment_account.payment_gateway == payment_gateway.name)
		.select(
			mode_of_payment.icon,
			mode_of_payment_account.payment_gateway,
			mode_of_payment.portal_title.as_("title"),
			mode_of_payment.name,
			mode_of_payment.portal_description,
		)
		.where(
			mode_of_payment.enabled
			& (mode_of_payment_account.company == company)
			& (payment_gateway.disabled == 0)
		)
	)

	return query.run(as_dict=True)
