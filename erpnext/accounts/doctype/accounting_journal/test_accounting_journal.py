# Copyright (c) 2019, Frappe Technologies Pvt. Ltd. and Contributors
# See license.txt


from unittest.mock import patch

import frappe
from frappe.tests import IntegrationTestCase
from frappe.tests.utils import make_test_objects
from frappe.utils import nowdate

import erpnext.accounts.doctype.journal_entry.journal_entry
from erpnext.accounts.doctype.accounting_journal.accounting_journal import (
	accounting_journal_adjustment,
)
from erpnext.accounts.doctype.payment_entry.test_payment_entry import get_payment_entry


class TestAccountingJournal(IntegrationTestCase):
	@classmethod
	def setUpClass(cls):
		frappe.get_doc(
			{
				"doctype": "Accounting Journal",
				"journal_code": "BQ",
				"journal_name": "Banque",
				"type": "Bank",
				"account": "_Test Bank - _TC",
				"company": "_Test Company",
				"conditions": [
					{"document_type": "Payment Entry"},
				],
			}
		).insert(ignore_if_duplicate=True)

		frappe.get_doc(
			{
				"doctype": "Accounting Journal",
				"journal_code": "MD",
				"journal_name": "Miscellaneous Operations",
				"type": "Miscellaneous",
				"company": "_Test Company",
			}
		).insert(ignore_if_duplicate=True)

	def make(self):
		test_records = make_test_objects("Sales Invoice")
		si = frappe.get_doc("Sales Invoice", test_records[0])
		si.is_pos = 0
		si.submit()
		return si

	def test_payment_entry_journal_adjustment(self):
		sales_invoice = self.make()

		pe = get_payment_entry("Sales Invoice", sales_invoice.name, bank_account="_Test Bank - _TC")
		pe.reference_no = "1"
		pe.reference_date = nowdate()
		pe.paid_from_account_currency = sales_invoice.currency
		pe.paid_to_account_currency = sales_invoice.currency
		pe.source_exchange_rate = 1
		pe.target_exchange_rate = 1
		pe.paid_amount = sales_invoice.outstanding_amount
		pe.insert()
		pe.submit()

		si_status = frappe.db.get_value("Sales Invoice", sales_invoice.name, "status")
		self.assertEqual(si_status, "Paid")

		pe_gl_entries = frappe.get_all(
			"GL Entry",
			filters={"voucher_type": "Payment Entry", "voucher_no": pe.name},
			fields=["name", "accounting_journal"],
		)
		accounting_journal = next(iter(set(gl.accounting_journal for gl in pe_gl_entries)))
		self.assertEqual(accounting_journal, "BQ - _TC")

		accounting_journal_adjustment("Payment Entry", [pe.name], "MD - _TC")

		pe_gl_entries = frappe.get_all(
			"GL Entry",
			filters={"voucher_type": "Payment Entry", "voucher_no": pe.name, "is_cancelled": 0},
			fields=["name", "accounting_journal"],
		)
		accounting_journal = next(iter(set(gl.accounting_journal for gl in pe_gl_entries)))
		self.assertEqual(accounting_journal, "MD - _TC")

		si_status = frappe.db.get_value("Sales Invoice", sales_invoice.name, "status")
		self.assertEqual(si_status, "Paid")

	def make_journal_entry_multi_journals(self):
		return frappe.get_doc(
			{
				"doctype": "Journal Entry",
				"company": "_Test Company",
				"posting_date": nowdate(),
				"multi_currency": 1,  # to avoid validation error
				"accounts": [
					{
						"account": "_Test Bank - _TC",
						"debit_in_account_currency": 100,
						"accounting_journal": "BQ - _TC",
					},
					{
						"account": "_Test Bank - _TC",
						"credit_in_account_currency": 100,
						"accounting_journal": "MD - _TC",
					},
				],
			}
		)

	@IntegrationTestCase.change_settings("Accounts Settings", {"force_unique_journal_in_transaction": 1})
	def test_force_unique_journal_in_journal_entry(self):
		with self.assertRaisesRegex(
			frappe.ValidationError,
			"Your entries are linked to different journals. Please make sure it is correct.",
		):
			self.make_journal_entry_multi_journals().insert()

	@IntegrationTestCase.change_settings("Accounts Settings", {"force_unique_journal_in_transaction": 1})
	@patch.object(
		erpnext.accounts.doctype.journal_entry.journal_entry.JournalEntry,
		"validate_accounting_journals",
		lambda *args, **kwargs: None,
	)
	def test_force_unique_journal_in_transaction(self):
		with self.assertRaisesRegex(
			frappe.ValidationError,
			"Multiple accounting journals found in the same transaction: BQ - _TC, MD - _TC",
		):
			self.make_journal_entry_multi_journals().submit()
