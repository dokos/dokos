# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors and Contributors
# See license.txt
import unittest

import frappe
from frappe.tests import IntegrationTestCase


class TestSalesTaxesandChargesTemplate(IntegrationTestCase):
	@IntegrationTestCase.change_settings("Global Defaults", {"default_company": "_Test Company"})
	@IntegrationTestCase.change_settings(
		"Accounts Settings", {"allow_multi_currency_invoices_against_single_party_account": 1}
	)
	def test_item_tax(self):
		si = frappe.new_doc("Sales Invoice")
		si.update(
			{
				"customer": "_Test Customer",
				"posting_date": "2023-01-01",
				"taxes_and_charges": None,
				"items": [
					{
						"item_code": "_Test Item",
						"qty": 1,
						"rate": 100,
						"item_tax_template": "_Test Item Tax Template 2 - _TC",
					}
				],
			}
		)
		si.set_taxes()
		si.save()

		self.assertEqual(si.total_taxes_and_charges, 20.0)
		self.assertEqual(si.grand_total, 120.0)

	@IntegrationTestCase.change_settings("Global Defaults", {"default_company": "_Test Company"})
	@IntegrationTestCase.change_settings(
		"Accounts Settings", {"allow_multi_currency_invoices_against_single_party_account": 1}
	)
	def test_item_tax_with_discount(self):
		si = frappe.new_doc("Sales Invoice")
		si.update(
			{
				"customer": "_Test Customer",
				"posting_date": "2023-01-01",
				"taxes_and_charges": None,
				"items": [
					{
						"item_code": "_Test Item",
						"qty": 1,
						"rate": 100,
						"item_tax_template": "_Test Item Tax Template 2 - _TC",
					}
				],
			}
		)

		si.apply_discount_on = "Net Total"
		si.discount_amount = 50

		si.set_taxes()
		si.save()

		self.assertEqual(si.total_taxes_and_charges, 10.0)
		self.assertEqual(si.grand_total, 60.0)

	# @IntegrationTestCase.change_settings("Global Defaults", {"default_company": "_Test Company"})
	# def test_item_tax_mixed(self):
	# 	self.skipTest("WIP: Compute invoice-level tax only on untaxed (item tax) amount.")
	# 	si = frappe.new_doc("Sales Invoice")
	# 	si.update(
	# 		{
	# 			"customer": "_Test Customer",
	# 			"posting_date": "2023-01-01",
	# 			"taxes_and_charges": "_Test Sales Taxes and Charges Template - _TC",  # should be ignored
	# 			"items": [
	# 				{
	# 					"item_code": "_Test Item",
	# 					"qty": 1,
	# 					"rate": 100,
	# 					"item_tax_template": "_Test Item Tax Template 2 - _TC",
	# 				}
	# 			],
	# 		}
	# 	)
	# 	si.save()

	# 	self.assertEqual(si.total_taxes_and_charges, 20.0)
	# 	self.assertEqual(si.grand_total, 120.0)
