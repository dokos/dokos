import frappe


def execute():
	"""Fix default Success Action for French users of Quotation and other doctypes"""

	from erpnext.setup.default_success_action import doctype_list

	for doctype in doctype_list:
		old_msg = f"{frappe._(doctype)} a été envoyé avec succès"

		filters = {
			"ref_doctype": doctype,
			"first_success_message": old_msg,
			"message": old_msg,
		}
		for doc in frappe.get_all("Success Action", filters=filters):
			print(f"Updating Success Action for {doctype}: {doc.name}")
			new_msg = f"{frappe._(doctype)} a été validé avec succès"
			doc = frappe.get_doc("Success Action", doc.name)
			doc.first_success_message = new_msg
			doc.message = new_msg
			doc.save()
