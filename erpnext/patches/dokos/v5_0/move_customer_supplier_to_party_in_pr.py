import frappe


def execute():
	frappe.reload_doc("accounts", "doctype", "Payment Request")
	for payment_request in frappe.get_all("Payment Request", pluck="name"):
		if customer := frappe.db.get_value("Payment Request", payment_request, "customer"):
			set_value_in_pr(payment_request, "Customer", customer)

		if supplier := frappe.db.get_value("Payment Request", payment_request, "supplier"):
			set_value_in_pr(payment_request, "Supplier", supplier)


def set_value_in_pr(pr, party_type, party):
	frappe.db.set_value("Payment Request", pr, "party_type", party_type, update_modified=False)
	frappe.db.set_value("Payment Request", pr, "party", party, update_modified=False)
