import click
import frappe


def execute():
	if "bookings" not in frappe.get_installed_apps():
		notifications = frappe.get_all("Notification", {"module": "Venue", "is_standard": 1}, pluck="name")
		for notification in notifications:
			frappe.delete_doc("Notification", notification, ignore_missing=True, force=True)

		dashboard_charts = frappe.get_all(
			"Dashboard Chart", {"module": "Venue", "is_standard": 1}, pluck="name"
		)
		for dashboard_chart in dashboard_charts:
			frappe.delete_doc("Dashboard Chart", dashboard_chart, ignore_missing=True, force=True)

		reports = frappe.get_all("Report", {"module": "Venue", "is_standard": 1}, pluck="name")
		for report in reports:
			frappe.delete_doc("Report", report, ignore_missing=True, force=True)

		web_templates = frappe.get_all("Web Template", {"module": "Venue", "standard": 1}, pluck="name")
		for web_template in web_templates:
			frappe.delete_doc("Web Template", web_template, ignore_missing=True, force=True)

		doctypes = frappe.get_all("DocType", {"module": "Venue", "custom": 0}, pluck="name")
		for doctype in doctypes:
			frappe.delete_doc("DocType", doctype, ignore_missing=True, force=True)

	frappe.delete_doc("Workspace", "Venue", ignore_missing=True, force=True)

	frappe.delete_doc("Module Def", "Venue", ignore_missing=True, force=True)

	click.secho(
		"Venue related features were moved to a separate app.\n"
		"Please install the Bookings app to continue using the features from the Venue module: https://gitlab.com/dokos/bookings",
		fg="yellow",
	)
