import frappe


def execute():
	# Delete assigned roles
	roles = ["Hotel Manager", "Hotel Reservation User", "Restaurant Manager"]
	doctypes = [
		x["name"] for x in frappe.get_all("DocType", filters={"module": ["in", ["Restaurant", "Hotels"]]})
	]

	try:
		frappe.db.delete("Has Role", {"role": ("in", roles)})
	except Exception as e:
		print(e)

	# Standard portal items
	try:
		frappe.db.delete("Portal Menu Item", {"reference_doctype": ("in", doctypes)})
	except Exception as e:
		print(e)

	# Delete DocTypes, Pages, Reports, Roles, Domain and Custom Fields
	elements = [
		{
			"document": "Custom Field",
			"items": [
				"Sales Invoice-restaurant",
				"Sales Invoice-restaurant_table",
				"Price List-restaurant_menu",
			],
		},
		{
			"document": "Report",
			"items": [x["name"] for x in frappe.get_all("Report", filters={"ref_doctype": ["in", doctypes]})],
		},
		{"document": "DocType", "items": doctypes},
		{
			"document": "Page",
			"items": [
				x["name"]
				for x in frappe.get_all("Page", filters={"module": ["in", ["Restaurant", "Hotels"]]})
			],
		},
		{"document": "Role", "items": roles},
		{"document": "Module Def", "items": ["Restaurant", "Hotels"]},
		{"document": "Domain", "items": ["Hospitality"]},
	]

	for element in elements:
		for item in element["items"]:
			try:
				frappe.delete_doc(element["document"], item)
			except Exception as e:
				print(e)

	# Delete Desktop Icons
	desktop_icons = ["Hotels", "Restaurant"]
	try:
		frappe.db.delete("Desktop Icon", {"module_name": ("in", desktop_icons)})
	except Exception as e:
		print(e)
