import frappe
from frappe import _


def execute():
	# Delete assigned roles
	roles = ["Student", "Instructor", "Academics User", "Education Manager", "Guardian"]
	doctypes = [x["name"] for x in frappe.get_all("DocType", filters={"module": "Education"})]

	try:
		frappe.db.delete("Has Role", {"role": ("in", roles)})
	except Exception as e:
		print(e)

	try:
		frappe.db.delete("DocPerm", {"role": ("in", roles)})
	except Exception as e:
		print(e)

	# Standard portal items
	try:
		frappe.db.delete("Portal Menu Item", {"reference_doctype": ("in", doctypes)})
	except Exception as e:
		print(e)

	# Delete DocTypes, Pages, Reports, Roles, Domain and Custom Fields

	elements = [
		{
			"document": "Web Form",
			"items": [x["name"] for x in frappe.get_all("Web Form", filters={"module": "Education"})],
		},
		{
			"document": "Report",
			"items": [x["name"] for x in frappe.get_all("Report", filters={"ref_doctype": ["in", doctypes]})],
		},
		{"document": "DocType", "items": doctypes},
		{
			"document": "Page",
			"items": [x["name"] for x in frappe.get_all("Page", filters={"module": "Education"})],
		},
		{"document": "Role", "items": roles},
		{"document": "Module Def", "items": ["Education"]},
		{"document": "Domain", "items": ["Education"]},
	]

	for element in elements:
		for item in element["items"]:
			try:
				frappe.delete_doc(element["document"], item)
			except Exception as e:
				print(e)

	# Delete Desktop Icons
	desktop_icons = ["Student", "Program", "Course", "Student Group", "Instructor", "Fees"]
	try:
		frappe.db.delete("Desktop Icon", {"module_name": ("in", desktop_icons)})
	except Exception as e:
		print(e)
