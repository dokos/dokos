import frappe


def execute():
	frappe.reload_doc("Assets", "DocType", "Location")
	# Delete assigned roles
	roles = ["Agriculture Manager", "Agriculture User"]
	doctypes = [x["name"] for x in frappe.get_all("DocType", filters={"module": "Agriculture"})]

	try:
		frappe.db.delete("Has Role", {"role": ("in", roles)})
	except Exception as e:
		print(e)

	# Standard portal items
	try:
		frappe.db.delete("Portal Menu Item", {"reference_doctype": ("in", doctypes)})
	except Exception as e:
		print(e)

	# Delete DocTypes, Pages, Reports, Roles, Domain and Custom Fields
	elements = [
		{"document": "Item Group", "items": ["Fertilizer", "Seed", "By-product", "Produce"]},
		{
			"document": "Report",
			"items": [x["name"] for x in frappe.get_all("Report", filters={"ref_doctype": ["in", doctypes]})],
		},
		{"document": "DocType", "items": doctypes},
		{"document": "Role", "items": roles},
		{"document": "Module Def", "items": ["Agriculture"]},
		{"document": "Domain", "items": ["Agriculture"]},
	]

	for element in elements:
		for item in element["items"]:
			try:
				frappe.delete_doc(element["document"], item)
			except Exception as e:
				print(e)

	# Delete Desktop Icons
	desktop_icons = [
		"Agriculture Task",
		"Crop",
		"Crop Cycle",
		"Fertilizer",
		"Item",
		"Location",
		"Disease",
		"Plant Analysis",
		"Soil Analysis",
		"Soil Texture",
		"Task",
		"Water Analysis",
		"Weather",
	]
	try:
		frappe.db.delete("Desktop Icon", {"module_name": ("in", desktop_icons)})
	except Exception as e:
		print(e)
