import frappe
from frappe import _


def execute():
	# Delete assigned roles
	roles = [
		"Healthcare Administrator",
		"LabTest Approver",
		"Laboratory User",
		"Nursing User",
		"Physician",
		"Patient",
	]
	doctypes = [x["name"] for x in frappe.get_all("DocType", filters={"module": "Healthcare"})]

	try:
		frappe.db.delete("Has Role", {"role": ("in", roles)})
	except Exception as e:
		print(e)

	# Standard portal items
	try:
		frappe.db.delete("Portal Menu Item", {"reference_doctype": ("in", doctypes)})
	except Exception as e:
		print(e)

	# Delete DocTypes, Pages, Reports, Roles, Domain and Custom Fields

	elements = [
		{"document": "Item Group", "items": [_("Laboratory"), _("Drug")]},
		{
			"document": "Custom Field",
			"items": [x["name"] for x in frappe.get_all("Custom Field", filters={"dt": ["in", doctypes]})],
		},
		{
			"document": "Web Form",
			"items": [x["name"] for x in frappe.get_all("Web Form", filters={"module": "Healthcare"})],
		},
		{
			"document": "Print Format",
			"items": [
				x["name"] for x in frappe.get_all("Print Format", filters={"doc_type": ["in", doctypes]})
			],
		},
		{
			"document": "Report",
			"items": [x["name"] for x in frappe.get_all("Report", filters={"ref_doctype": ["in", doctypes]})],
		},
		{"document": "DocType", "items": doctypes},
		{
			"document": "Page",
			"items": [x["name"] for x in frappe.get_all("Page", filters={"module": "Healthcare"})],
		},
		{"document": "Role", "items": roles},
		{"document": "Module Def", "items": ["Healthcare"]},
		{"document": "Domain", "items": ["Healthcare"]},
	]

	for element in elements:
		for item in element["items"]:
			try:
				frappe.delete_doc(element["document"], item)
			except Exception as e:
				print(e)

	# Delete Desktop Icons
	desktop_icons = [
		"Patient",
		"Patient Appointment",
		"Patient Encounter",
		"Lab Test",
		"Healthcare",
		"Vital Signs",
		"Clinical Procedure",
		"Inpatient Record",
	]
	try:
		frappe.db.delete("Desktop Icon", {"module_name": ("in", desktop_icons)})
	except Exception as e:
		print(e)
