import frappe


def execute():
	frappe.reload_doc("selling", "doctype", "customer")
	frappe.reload_doc("selling", "doctype", "payment_gateways_references")

	for ir in frappe.db.sql("select name from `tabIntegration References`", as_dict=True):
		doc = frappe.db.get_value("Integration References", ir.name, "*", as_dict=True)
		customer = frappe.get_doc("Customer", doc.customer)

		if not doc.stripe_customer_id and not doc.gocardless_customer_id:
			return

		if doc.stripe_customer_id:
			customer.append(
				"payment_gateways_references",
				{
					"payment_gateway": frappe.db.get_value(
						"Payment Gateway",
						dict(gateway_settings="Stripe Settings", gateway_controller=doc.stripe_settings, disabled=0),
					),
					"customer_id": doc.stripe_customer_id,
				},
			)

		if doc.gocardless_customer_id:
			customer.append(
				"payment_gateways_references",
				{
					"payment_gateway": frappe.db.get_value(
						"Payment Gateway",
						dict(
							gateway_settings="GoCardless Settings",
							gateway_controller=doc.gocardless_settings,
							disabled=0,
						),
					),
					"customer_id": doc.gocardless_customer_id,
				},
			)

		customer.flags.ignore_mandatory = True
		customer.save()
