import frappe


def execute():
	for subscription in frappe.get_all("Subscription"):
		sub = frappe.get_doc("Subscription", subscription.name)
		if not sub.get_state_value("sales_invoice"):
			if si := frappe.db.get_value(
				"Sales Invoice",
				dict(
					docstatus=1,
					subscription=sub.name,
					from_date=sub.current_invoice_start,
					to_date=sub.current_invoice_end,
				),
			):
				sub.set_state("sales_invoice", si)
