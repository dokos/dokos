import frappe


def execute():
	for field in [
		"fee_account",
		"fees",
		"mode_of_payment",
		"cost_center",
		"tax_account",
		"column_break_7",
	]:
		if fn := frappe.db.get_value("Custom Field", dict(fieldname=field, dt="Payment Gateway")):
			frappe.delete_doc("Custom Field", fn)
