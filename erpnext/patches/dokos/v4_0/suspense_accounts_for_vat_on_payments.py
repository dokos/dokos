import frappe

from erpnext.regional.france.setup import setup


def execute():
	companies = frappe.get_all("Company", filters={"country": "France"})
	if not companies:
		return

	setup()

	for acc in [
		{"account_number": "445742", "account_name": "TVA collectée sur encaissements - 20%", "tax_rate": 20},
		{
			"account_number": "445642",
			"account_name": "TVA déductible sur encaissements - 20%",
			"tax_rate": 20,
		},
	]:
		for company in companies:
			if not frappe.db.exists(
				"Account", dict(account_number=acc["account_number"][:4], company=company.name, is_group=1)
			):
				continue

			account = frappe.new_doc("Account")
			account.account_name = acc["account_name"]
			account.account_number = acc["account_number"]
			account.parent_account = frappe.db.get_value(
				"Account",
				filters=dict(account_number=acc["account_number"][:4], is_group=1, company=company.name),
			)
			account.account_type = "Tax"
			account.tax_rate = acc["tax_rate"]
			account.company = company.name
			account.insert(ignore_if_duplicate=True)

	for company in companies:
		try:
			doc = frappe.get_doc("Company", company)
			doc.flags.ignore_mandatory = True
			doc.flags.ignore_links = True
			doc.save()
		except Exception:
			continue
