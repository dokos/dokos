import frappe


def execute():
	for invoice in frappe.get_all("Sales Invoice", filters={"is_down_payment_invoice": 1, "docstatus": 1}):
		for payment in frappe.get_all(
			"Payment Entry Reference",
			filters={"reference_doctype": "Sales Invoice", "reference_name": invoice.name},
			pluck="parent",
		):
			doc = frappe.get_doc("Payment Entry", payment)
			doc.run_method("update_advance_paid")
