import frappe


def execute():
	meta = frappe.get_meta("Mode of Payment")
	if not meta.has_field("payment_gateway"):
		return

	mops = frappe.get_all(
		"Mode of Payment", filters={"payment_gateway": ("is", "set")}, fields=["name", "payment_gateway"]
	)
	frappe.reload_doc("accounts", "doctype", "Mode of Payment")
	frappe.reload_doc("accounts", "doctype", "Mode of Payment Account")

	for mop in mops:
		doc = frappe.get_doc("Mode of Payment", mop.name)
		for row in doc.accounts:
			frappe.db.set_value("Mode of Payment Account", row.name, "payment_gateway", mop.payment_gateway)
