import frappe


def execute():
	for pr in frappe.get_all("Payment Request"):
		doc = frappe.get_doc("Payment Request", pr.name)
		if customer := doc.get_customer():
			frappe.db.set_value("Payment Request", pr.name, "customer", customer, update_modified=False)
