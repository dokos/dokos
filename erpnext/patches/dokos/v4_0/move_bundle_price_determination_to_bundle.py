import frappe


def execute():
	if frappe.db.get_single_value("Selling Settings", "editable_bundle_item_rates"):
		for bundle in frappe.get_all("Product Bundle"):
			frappe.db.set_value("Product Bundle", bundle.name, "editable_bundle_item_rates", 1)
