import frappe
import yaml


def get_data(domain):
	path = frappe.get_app_path("erpnext", "domains", frappe.scrub(domain) + ".yaml")

	with open(path) as stream:
		try:
			return yaml.safe_load(stream) or {}
		except yaml.YAMLError as exc:
			frappe.log_error("Domain Setup Error", exc)
			return {}
