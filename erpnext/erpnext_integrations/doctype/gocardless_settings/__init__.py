import frappe


@frappe.whitelist(allow_guest=True)
def webhooks():
	from payments.payment_gateways.doctype.gocardless_settings import webhooks

	return webhooks()
